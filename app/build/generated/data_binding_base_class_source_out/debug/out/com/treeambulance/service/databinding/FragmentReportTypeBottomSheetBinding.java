// Generated by data binding compiler. Do not edit!
package com.treeambulance.service.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.treeambulance.service.R;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentReportTypeBottomSheetBinding extends ViewDataBinding {
  @NonNull
  public final RecyclerView rvReportTye;

  @NonNull
  public final TextView tvSheetTitle;

  @Bindable
  protected View.OnClickListener mClickListener;

  protected FragmentReportTypeBottomSheetBinding(Object _bindingComponent, View _root,
      int _localFieldCount, RecyclerView rvReportTye, TextView tvSheetTitle) {
    super(_bindingComponent, _root, _localFieldCount);
    this.rvReportTye = rvReportTye;
    this.tvSheetTitle = tvSheetTitle;
  }

  public abstract void setClickListener(@Nullable View.OnClickListener clickListener);

  @Nullable
  public View.OnClickListener getClickListener() {
    return mClickListener;
  }

  @NonNull
  public static FragmentReportTypeBottomSheetBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_report_type_bottom_sheet, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentReportTypeBottomSheetBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentReportTypeBottomSheetBinding>inflateInternal(inflater, R.layout.fragment_report_type_bottom_sheet, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentReportTypeBottomSheetBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_report_type_bottom_sheet, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentReportTypeBottomSheetBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentReportTypeBottomSheetBinding>inflateInternal(inflater, R.layout.fragment_report_type_bottom_sheet, null, false, component);
  }

  public static FragmentReportTypeBottomSheetBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentReportTypeBottomSheetBinding bind(@NonNull View view,
      @Nullable Object component) {
    return (FragmentReportTypeBottomSheetBinding)bind(component, view, R.layout.fragment_report_type_bottom_sheet);
  }
}
