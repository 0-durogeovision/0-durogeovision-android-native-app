// Generated by data binding compiler. Do not edit!
package com.treeambulance.service.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.button.MaterialButton;
import com.treeambulance.service.R;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentAddPlantationBinding extends ViewDataBinding {
  @NonNull
  public final MaterialButton btAddProject;

  @NonNull
  public final EditText etCountryName;

  @NonNull
  public final EditText etDistrict;

  @NonNull
  public final EditText etLocationName;

  @NonNull
  public final EditText etProject;

  @NonNull
  public final EditText etState;

  @NonNull
  public final EditText etVillage;

  @NonNull
  public final ImageView ivBackAddProject;

  @NonNull
  public final LinearLayout llCountryName;

  @NonNull
  public final LinearLayout llDistrictName;

  @NonNull
  public final LinearLayout llLocationName;

  @NonNull
  public final LinearLayout llProjectName;

  @NonNull
  public final LinearLayout llStateName;

  @NonNull
  public final LinearLayout llVillageName;

  @NonNull
  public final RelativeLayout rlToolBar;

  @NonNull
  public final RecyclerView rvPlantationList;

  @NonNull
  public final TextView tvNoData;

  @NonNull
  public final TextView tvNotes;

  @NonNull
  public final TextView tvTitle;

  @Bindable
  protected View.OnClickListener mClickListener;

  protected FragmentAddPlantationBinding(Object _bindingComponent, View _root, int _localFieldCount,
      MaterialButton btAddProject, EditText etCountryName, EditText etDistrict,
      EditText etLocationName, EditText etProject, EditText etState, EditText etVillage,
      ImageView ivBackAddProject, LinearLayout llCountryName, LinearLayout llDistrictName,
      LinearLayout llLocationName, LinearLayout llProjectName, LinearLayout llStateName,
      LinearLayout llVillageName, RelativeLayout rlToolBar, RecyclerView rvPlantationList,
      TextView tvNoData, TextView tvNotes, TextView tvTitle) {
    super(_bindingComponent, _root, _localFieldCount);
    this.btAddProject = btAddProject;
    this.etCountryName = etCountryName;
    this.etDistrict = etDistrict;
    this.etLocationName = etLocationName;
    this.etProject = etProject;
    this.etState = etState;
    this.etVillage = etVillage;
    this.ivBackAddProject = ivBackAddProject;
    this.llCountryName = llCountryName;
    this.llDistrictName = llDistrictName;
    this.llLocationName = llLocationName;
    this.llProjectName = llProjectName;
    this.llStateName = llStateName;
    this.llVillageName = llVillageName;
    this.rlToolBar = rlToolBar;
    this.rvPlantationList = rvPlantationList;
    this.tvNoData = tvNoData;
    this.tvNotes = tvNotes;
    this.tvTitle = tvTitle;
  }

  public abstract void setClickListener(@Nullable View.OnClickListener clickListener);

  @Nullable
  public View.OnClickListener getClickListener() {
    return mClickListener;
  }

  @NonNull
  public static FragmentAddPlantationBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_add_plantation, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentAddPlantationBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentAddPlantationBinding>inflateInternal(inflater, R.layout.fragment_add_plantation, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentAddPlantationBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_add_plantation, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentAddPlantationBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentAddPlantationBinding>inflateInternal(inflater, R.layout.fragment_add_plantation, null, false, component);
  }

  public static FragmentAddPlantationBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentAddPlantationBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentAddPlantationBinding)bind(component, view, R.layout.fragment_add_plantation);
  }
}
