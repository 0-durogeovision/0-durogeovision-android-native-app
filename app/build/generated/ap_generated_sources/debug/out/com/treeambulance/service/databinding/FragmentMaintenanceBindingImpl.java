package com.treeambulance.service.databinding;
import com.treeambulance.service.R;
import com.treeambulance.service.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentMaintenanceBindingImpl extends FragmentMaintenanceBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.rlToolBar, 13);
        sViewsWithIds.put(R.id.tvTitle, 14);
        sViewsWithIds.put(R.id.llProjectName, 15);
        sViewsWithIds.put(R.id.llNewTreeNo, 16);
        sViewsWithIds.put(R.id.etNewTreeNo, 17);
        sViewsWithIds.put(R.id.llPreTree1, 18);
        sViewsWithIds.put(R.id.ivPreTree1st, 19);
        sViewsWithIds.put(R.id.llTreeNo, 20);
        sViewsWithIds.put(R.id.etTreeNo, 21);
        sViewsWithIds.put(R.id.llLandSurveyNo, 22);
        sViewsWithIds.put(R.id.etLangSurveyNo, 23);
        sViewsWithIds.put(R.id.llMaintenanceDate, 24);
        sViewsWithIds.put(R.id.llOperatorName, 25);
        sViewsWithIds.put(R.id.llMaintenanceStatus, 26);
        sViewsWithIds.put(R.id.autoCompleteTextView, 27);
        sViewsWithIds.put(R.id.llStatusTree, 28);
        sViewsWithIds.put(R.id.etStatusTree, 29);
        sViewsWithIds.put(R.id.llTree1, 30);
        sViewsWithIds.put(R.id.llTree2, 31);
        sViewsWithIds.put(R.id.llSelfiePhoto, 32);
        sViewsWithIds.put(R.id.tvNotes, 33);
        sViewsWithIds.put(R.id.etNotes, 34);
    }
    // views
    @NonNull
    private final android.widget.FrameLayout mboundView0;
    @NonNull
    private final android.widget.FrameLayout mboundView10;
    @NonNull
    private final android.widget.FrameLayout mboundView6;
    @NonNull
    private final android.widget.FrameLayout mboundView8;
    // variables
    // values
    // listeners
    private OnClickListenerImpl mClickListenerOnClickAndroidViewViewOnClickListener;
    // Inverse Binding Event Handlers

    public FragmentMaintenanceBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 35, sIncludes, sViewsWithIds));
    }
    private FragmentMaintenanceBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.AutoCompleteTextView) bindings[27]
            , (com.google.android.material.button.MaterialButton) bindings[12]
            , (com.google.android.material.button.MaterialButton) bindings[3]
            , (android.widget.EditText) bindings[23]
            , (android.widget.EditText) bindings[4]
            , (android.widget.EditText) bindings[17]
            , (android.widget.EditText) bindings[34]
            , (android.widget.EditText) bindings[5]
            , (android.widget.EditText) bindings[2]
            , (android.widget.EditText) bindings[29]
            , (android.widget.EditText) bindings[21]
            , (android.widget.ImageView) bindings[1]
            , (android.widget.ImageView) bindings[19]
            , (android.widget.ImageView) bindings[11]
            , (android.widget.ImageView) bindings[7]
            , (android.widget.ImageView) bindings[9]
            , (android.widget.LinearLayout) bindings[22]
            , (android.widget.LinearLayout) bindings[24]
            , (android.widget.LinearLayout) bindings[26]
            , (android.widget.LinearLayout) bindings[16]
            , (android.widget.LinearLayout) bindings[25]
            , (android.widget.LinearLayout) bindings[18]
            , (android.widget.LinearLayout) bindings[15]
            , (android.widget.LinearLayout) bindings[32]
            , (android.widget.LinearLayout) bindings[28]
            , (android.widget.LinearLayout) bindings[30]
            , (android.widget.LinearLayout) bindings[31]
            , (android.widget.LinearLayout) bindings[20]
            , (android.widget.RelativeLayout) bindings[13]
            , (android.widget.TextView) bindings[33]
            , (android.widget.TextView) bindings[14]
            );
        this.btSave.setTag(null);
        this.btSubmit.setTag(null);
        this.etMaintenanceDate.setTag(null);
        this.etOperatorName.setTag(null);
        this.etProjectName.setTag(null);
        this.ivBackMaintenance.setTag(null);
        this.ivSelfiePhoto.setTag(null);
        this.ivTree1st.setTag(null);
        this.ivTree2nd.setTag(null);
        this.mboundView0 = (android.widget.FrameLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView10 = (android.widget.FrameLayout) bindings[10];
        this.mboundView10.setTag(null);
        this.mboundView6 = (android.widget.FrameLayout) bindings[6];
        this.mboundView6.setTag(null);
        this.mboundView8 = (android.widget.FrameLayout) bindings[8];
        this.mboundView8.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.clickListener == variableId) {
            setClickListener((android.view.View.OnClickListener) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setClickListener(@Nullable android.view.View.OnClickListener ClickListener) {
        this.mClickListener = ClickListener;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.clickListener);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        android.view.View.OnClickListener clickListenerOnClickAndroidViewViewOnClickListener = null;
        android.view.View.OnClickListener clickListener = mClickListener;

        if ((dirtyFlags & 0x3L) != 0) {



                if (clickListener != null) {
                    // read clickListener::onClick
                    clickListenerOnClickAndroidViewViewOnClickListener = (((mClickListenerOnClickAndroidViewViewOnClickListener == null) ? (mClickListenerOnClickAndroidViewViewOnClickListener = new OnClickListenerImpl()) : mClickListenerOnClickAndroidViewViewOnClickListener).setValue(clickListener));
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            this.btSave.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.btSubmit.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etMaintenanceDate.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etOperatorName.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etProjectName.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.ivBackMaintenance.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.ivSelfiePhoto.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.ivTree1st.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.ivTree2nd.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.mboundView10.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.mboundView6.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.mboundView8.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
        }
    }
    // Listener Stub Implementations
    public static class OnClickListenerImpl implements android.view.View.OnClickListener{
        private android.view.View.OnClickListener value;
        public OnClickListenerImpl setValue(android.view.View.OnClickListener value) {
            this.value = value;
            return value == null ? null : this;
        }
        @Override
        public void onClick(android.view.View arg0) {
            this.value.onClick(arg0); 
        }
    }
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): clickListener
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}