package com.treeambulance.service;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.treeambulance.service.databinding.ActivityDataRetrievalBindingImpl;
import com.treeambulance.service.databinding.ActivityHomeBindingImpl;
import com.treeambulance.service.databinding.ActivityPlantationRetrievalBindingImpl;
import com.treeambulance.service.databinding.ActivitySelectCompanyListBindingImpl;
import com.treeambulance.service.databinding.ActivitySplashBindingImpl;
import com.treeambulance.service.databinding.AdapterAllProjectBindingImpl;
import com.treeambulance.service.databinding.AdapterBluetoothDeviceBindingImpl;
import com.treeambulance.service.databinding.AdapterCompanyListBindingImpl;
import com.treeambulance.service.databinding.AdapterCountryListBindingImpl;
import com.treeambulance.service.databinding.AdapterCurrencyNameBindingImpl;
import com.treeambulance.service.databinding.AdapterEditSelectedBluetoothDeviceBindingImpl;
import com.treeambulance.service.databinding.AdapterListBindingImpl;
import com.treeambulance.service.databinding.AdapterMultiListBindingImpl;
import com.treeambulance.service.databinding.AdapterMyOrdersListBindingImpl;
import com.treeambulance.service.databinding.AdapterOperatorListBindingImpl;
import com.treeambulance.service.databinding.AdapterOperatorsListBindingImpl;
import com.treeambulance.service.databinding.AdapterPlantationListBindingImpl;
import com.treeambulance.service.databinding.AdapterRangeListBindingImpl;
import com.treeambulance.service.databinding.AdapterReportTypeBindingImpl;
import com.treeambulance.service.databinding.AdapterRfidListBindingImpl;
import com.treeambulance.service.databinding.AdapterSelectedBluetoothDeviceBindingImpl;
import com.treeambulance.service.databinding.AdapterStateListBindingImpl;
import com.treeambulance.service.databinding.AdapterStatusRangeBindingImpl;
import com.treeambulance.service.databinding.AdapterTreeListBindingImpl;
import com.treeambulance.service.databinding.AppBarMainBindingImpl;
import com.treeambulance.service.databinding.DummyBindingImpl;
import com.treeambulance.service.databinding.FragmentAddPlantationBindingImpl;
import com.treeambulance.service.databinding.FragmentAddProjectBindingImpl;
import com.treeambulance.service.databinding.FragmentAddTreeManagementBindingImpl;
import com.treeambulance.service.databinding.FragmentAllProjectsBindingImpl;
import com.treeambulance.service.databinding.FragmentBluetoothDeviceBottomSheetBindingImpl;
import com.treeambulance.service.databinding.FragmentChangePasswordBottomSheetBindingImpl;
import com.treeambulance.service.databinding.FragmentCityMultiListBottomSheetBindingImpl;
import com.treeambulance.service.databinding.FragmentCountryListBottomSheetBindingImpl;
import com.treeambulance.service.databinding.FragmentCreateCompanyBindingImpl;
import com.treeambulance.service.databinding.FragmentCurrencyNameBottomSheetBindingImpl;
import com.treeambulance.service.databinding.FragmentCustomDialogBottomSheetBindingImpl;
import com.treeambulance.service.databinding.FragmentDataRetrievalBindingImpl;
import com.treeambulance.service.databinding.FragmentDummyBindingImpl;
import com.treeambulance.service.databinding.FragmentDuroTekModeBindingImpl;
import com.treeambulance.service.databinding.FragmentEditCompanyBindingImpl;
import com.treeambulance.service.databinding.FragmentGpsReadingBottomSheetBindingImpl;
import com.treeambulance.service.databinding.FragmentLoginPasswordBottomSheetBindingImpl;
import com.treeambulance.service.databinding.FragmentMaintenanceBindingImpl;
import com.treeambulance.service.databinding.FragmentManageOperatorBindingImpl;
import com.treeambulance.service.databinding.FragmentModesBottomSheetBindingImpl;
import com.treeambulance.service.databinding.FragmentOperatorListBottomSheetBindingImpl;
import com.treeambulance.service.databinding.FragmentOperatorMultiListBottomSheetBindingImpl;
import com.treeambulance.service.databinding.FragmentPlantationBindingImpl;
import com.treeambulance.service.databinding.FragmentPlantationMaintenanceBindingImpl;
import com.treeambulance.service.databinding.FragmentPlantationMapViewBindingImpl;
import com.treeambulance.service.databinding.FragmentPlantationRetrievalBindingImpl;
import com.treeambulance.service.databinding.FragmentPlantationSecondBindingImpl;
import com.treeambulance.service.databinding.FragmentProjectHomeBindingImpl;
import com.treeambulance.service.databinding.FragmentProjectMapViewBindingImpl;
import com.treeambulance.service.databinding.FragmentRangeListBottomSheetBindingImpl;
import com.treeambulance.service.databinding.FragmentReportBindingImpl;
import com.treeambulance.service.databinding.FragmentReportGenerationBindingImpl;
import com.treeambulance.service.databinding.FragmentReportTypeBottomSheetBindingImpl;
import com.treeambulance.service.databinding.FragmentReportsBottomSheetBindingImpl;
import com.treeambulance.service.databinding.FragmentRfidReadingBottomSheetBindingImpl;
import com.treeambulance.service.databinding.FragmentStateListBottomSheetBindingImpl;
import com.treeambulance.service.databinding.FragmentStateMultiListBottomSheetBindingImpl;
import com.treeambulance.service.databinding.FragmentStatusMultiListBottomSheetBindingImpl;
import com.treeambulance.service.databinding.FragmentStatusRangeBottomSheetBindingImpl;
import com.treeambulance.service.databinding.FragmentSupervisorModeBindingImpl;
import com.treeambulance.service.databinding.FragmentSurveyingFirstBindingImpl;
import com.treeambulance.service.databinding.FragmentSurveyingSecondBindingImpl;
import com.treeambulance.service.databinding.FragmentTransplantationFirstBindingImpl;
import com.treeambulance.service.databinding.FragmentTransplantationSecondBindingImpl;
import com.treeambulance.service.databinding.FragmentVillageMultiListBottomSheetBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYDATARETRIEVAL = 1;

  private static final int LAYOUT_ACTIVITYHOME = 2;

  private static final int LAYOUT_ACTIVITYPLANTATIONRETRIEVAL = 3;

  private static final int LAYOUT_ACTIVITYSELECTCOMPANYLIST = 4;

  private static final int LAYOUT_ACTIVITYSPLASH = 5;

  private static final int LAYOUT_ADAPTERALLPROJECT = 6;

  private static final int LAYOUT_ADAPTERBLUETOOTHDEVICE = 7;

  private static final int LAYOUT_ADAPTERCOMPANYLIST = 8;

  private static final int LAYOUT_ADAPTERCOUNTRYLIST = 9;

  private static final int LAYOUT_ADAPTERCURRENCYNAME = 10;

  private static final int LAYOUT_ADAPTEREDITSELECTEDBLUETOOTHDEVICE = 11;

  private static final int LAYOUT_ADAPTERLIST = 12;

  private static final int LAYOUT_ADAPTERMULTILIST = 13;

  private static final int LAYOUT_ADAPTERMYORDERSLIST = 14;

  private static final int LAYOUT_ADAPTEROPERATORLIST = 15;

  private static final int LAYOUT_ADAPTEROPERATORSLIST = 16;

  private static final int LAYOUT_ADAPTERPLANTATIONLIST = 17;

  private static final int LAYOUT_ADAPTERRANGELIST = 18;

  private static final int LAYOUT_ADAPTERREPORTTYPE = 19;

  private static final int LAYOUT_ADAPTERRFIDLIST = 20;

  private static final int LAYOUT_ADAPTERSELECTEDBLUETOOTHDEVICE = 21;

  private static final int LAYOUT_ADAPTERSTATELIST = 22;

  private static final int LAYOUT_ADAPTERSTATUSRANGE = 23;

  private static final int LAYOUT_ADAPTERTREELIST = 24;

  private static final int LAYOUT_APPBARMAIN = 25;

  private static final int LAYOUT_DUMMY = 26;

  private static final int LAYOUT_FRAGMENTADDPLANTATION = 27;

  private static final int LAYOUT_FRAGMENTADDPROJECT = 28;

  private static final int LAYOUT_FRAGMENTADDTREEMANAGEMENT = 29;

  private static final int LAYOUT_FRAGMENTALLPROJECTS = 30;

  private static final int LAYOUT_FRAGMENTBLUETOOTHDEVICEBOTTOMSHEET = 31;

  private static final int LAYOUT_FRAGMENTCHANGEPASSWORDBOTTOMSHEET = 32;

  private static final int LAYOUT_FRAGMENTCITYMULTILISTBOTTOMSHEET = 33;

  private static final int LAYOUT_FRAGMENTCOUNTRYLISTBOTTOMSHEET = 34;

  private static final int LAYOUT_FRAGMENTCREATECOMPANY = 35;

  private static final int LAYOUT_FRAGMENTCURRENCYNAMEBOTTOMSHEET = 36;

  private static final int LAYOUT_FRAGMENTCUSTOMDIALOGBOTTOMSHEET = 37;

  private static final int LAYOUT_FRAGMENTDATARETRIEVAL = 38;

  private static final int LAYOUT_FRAGMENTDUMMY = 39;

  private static final int LAYOUT_FRAGMENTDUROTEKMODE = 40;

  private static final int LAYOUT_FRAGMENTEDITCOMPANY = 41;

  private static final int LAYOUT_FRAGMENTGPSREADINGBOTTOMSHEET = 42;

  private static final int LAYOUT_FRAGMENTLOGINPASSWORDBOTTOMSHEET = 43;

  private static final int LAYOUT_FRAGMENTMAINTENANCE = 44;

  private static final int LAYOUT_FRAGMENTMANAGEOPERATOR = 45;

  private static final int LAYOUT_FRAGMENTMODESBOTTOMSHEET = 46;

  private static final int LAYOUT_FRAGMENTOPERATORLISTBOTTOMSHEET = 47;

  private static final int LAYOUT_FRAGMENTOPERATORMULTILISTBOTTOMSHEET = 48;

  private static final int LAYOUT_FRAGMENTPLANTATION = 49;

  private static final int LAYOUT_FRAGMENTPLANTATIONMAINTENANCE = 50;

  private static final int LAYOUT_FRAGMENTPLANTATIONMAPVIEW = 51;

  private static final int LAYOUT_FRAGMENTPLANTATIONRETRIEVAL = 52;

  private static final int LAYOUT_FRAGMENTPLANTATIONSECOND = 53;

  private static final int LAYOUT_FRAGMENTPROJECTHOME = 54;

  private static final int LAYOUT_FRAGMENTPROJECTMAPVIEW = 55;

  private static final int LAYOUT_FRAGMENTRANGELISTBOTTOMSHEET = 56;

  private static final int LAYOUT_FRAGMENTREPORT = 57;

  private static final int LAYOUT_FRAGMENTREPORTGENERATION = 58;

  private static final int LAYOUT_FRAGMENTREPORTTYPEBOTTOMSHEET = 59;

  private static final int LAYOUT_FRAGMENTREPORTSBOTTOMSHEET = 60;

  private static final int LAYOUT_FRAGMENTRFIDREADINGBOTTOMSHEET = 61;

  private static final int LAYOUT_FRAGMENTSTATELISTBOTTOMSHEET = 62;

  private static final int LAYOUT_FRAGMENTSTATEMULTILISTBOTTOMSHEET = 63;

  private static final int LAYOUT_FRAGMENTSTATUSMULTILISTBOTTOMSHEET = 64;

  private static final int LAYOUT_FRAGMENTSTATUSRANGEBOTTOMSHEET = 65;

  private static final int LAYOUT_FRAGMENTSUPERVISORMODE = 66;

  private static final int LAYOUT_FRAGMENTSURVEYINGFIRST = 67;

  private static final int LAYOUT_FRAGMENTSURVEYINGSECOND = 68;

  private static final int LAYOUT_FRAGMENTTRANSPLANTATIONFIRST = 69;

  private static final int LAYOUT_FRAGMENTTRANSPLANTATIONSECOND = 70;

  private static final int LAYOUT_FRAGMENTVILLAGEMULTILISTBOTTOMSHEET = 71;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(71);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.activity_data_retrieval, LAYOUT_ACTIVITYDATARETRIEVAL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.activity_home, LAYOUT_ACTIVITYHOME);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.activity_plantation_retrieval, LAYOUT_ACTIVITYPLANTATIONRETRIEVAL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.activity_select_company_list, LAYOUT_ACTIVITYSELECTCOMPANYLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.activity_splash, LAYOUT_ACTIVITYSPLASH);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.adapter_all_project, LAYOUT_ADAPTERALLPROJECT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.adapter_bluetooth_device, LAYOUT_ADAPTERBLUETOOTHDEVICE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.adapter_company_list, LAYOUT_ADAPTERCOMPANYLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.adapter_country_list, LAYOUT_ADAPTERCOUNTRYLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.adapter_currency_name, LAYOUT_ADAPTERCURRENCYNAME);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.adapter_edit_selected_bluetooth_device, LAYOUT_ADAPTEREDITSELECTEDBLUETOOTHDEVICE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.adapter_list, LAYOUT_ADAPTERLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.adapter_multi_list, LAYOUT_ADAPTERMULTILIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.adapter_my_orders_list, LAYOUT_ADAPTERMYORDERSLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.adapter_operator_list, LAYOUT_ADAPTEROPERATORLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.adapter_operators_list, LAYOUT_ADAPTEROPERATORSLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.adapter_plantation_list, LAYOUT_ADAPTERPLANTATIONLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.adapter_range_list, LAYOUT_ADAPTERRANGELIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.adapter_report_type, LAYOUT_ADAPTERREPORTTYPE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.adapter_rfid_list, LAYOUT_ADAPTERRFIDLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.adapter_selected_bluetooth_device, LAYOUT_ADAPTERSELECTEDBLUETOOTHDEVICE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.adapter_state_list, LAYOUT_ADAPTERSTATELIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.adapter_status_range, LAYOUT_ADAPTERSTATUSRANGE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.adapter_tree_list, LAYOUT_ADAPTERTREELIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.app_bar_main, LAYOUT_APPBARMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.dummy, LAYOUT_DUMMY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_add_plantation, LAYOUT_FRAGMENTADDPLANTATION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_add_project, LAYOUT_FRAGMENTADDPROJECT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_add_tree_management, LAYOUT_FRAGMENTADDTREEMANAGEMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_all_projects, LAYOUT_FRAGMENTALLPROJECTS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_bluetooth_device_bottom_sheet, LAYOUT_FRAGMENTBLUETOOTHDEVICEBOTTOMSHEET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_change_password_bottom_sheet, LAYOUT_FRAGMENTCHANGEPASSWORDBOTTOMSHEET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_city_multi_list_bottom_sheet, LAYOUT_FRAGMENTCITYMULTILISTBOTTOMSHEET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_country_list_bottom_sheet, LAYOUT_FRAGMENTCOUNTRYLISTBOTTOMSHEET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_create_company, LAYOUT_FRAGMENTCREATECOMPANY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_currency_name_bottom_sheet, LAYOUT_FRAGMENTCURRENCYNAMEBOTTOMSHEET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_custom_dialog_bottom_sheet, LAYOUT_FRAGMENTCUSTOMDIALOGBOTTOMSHEET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_data_retrieval, LAYOUT_FRAGMENTDATARETRIEVAL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_dummy, LAYOUT_FRAGMENTDUMMY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_duro_tek_mode, LAYOUT_FRAGMENTDUROTEKMODE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_edit_company, LAYOUT_FRAGMENTEDITCOMPANY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_gps_reading_bottom_sheet, LAYOUT_FRAGMENTGPSREADINGBOTTOMSHEET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_login_password_bottom_sheet, LAYOUT_FRAGMENTLOGINPASSWORDBOTTOMSHEET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_maintenance, LAYOUT_FRAGMENTMAINTENANCE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_manage_operator, LAYOUT_FRAGMENTMANAGEOPERATOR);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_modes_bottom_sheet, LAYOUT_FRAGMENTMODESBOTTOMSHEET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_operator_list_bottom_sheet, LAYOUT_FRAGMENTOPERATORLISTBOTTOMSHEET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_operator_multi_list_bottom_sheet, LAYOUT_FRAGMENTOPERATORMULTILISTBOTTOMSHEET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_plantation, LAYOUT_FRAGMENTPLANTATION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_plantation_maintenance, LAYOUT_FRAGMENTPLANTATIONMAINTENANCE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_plantation_map_view, LAYOUT_FRAGMENTPLANTATIONMAPVIEW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_plantation_retrieval, LAYOUT_FRAGMENTPLANTATIONRETRIEVAL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_plantation_second, LAYOUT_FRAGMENTPLANTATIONSECOND);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_project_home, LAYOUT_FRAGMENTPROJECTHOME);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_project_map_view, LAYOUT_FRAGMENTPROJECTMAPVIEW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_range_list_bottom_sheet, LAYOUT_FRAGMENTRANGELISTBOTTOMSHEET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_report, LAYOUT_FRAGMENTREPORT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_report_generation, LAYOUT_FRAGMENTREPORTGENERATION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_report_type_bottom_sheet, LAYOUT_FRAGMENTREPORTTYPEBOTTOMSHEET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_reports_bottom_sheet, LAYOUT_FRAGMENTREPORTSBOTTOMSHEET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_rfid_reading_bottom_sheet, LAYOUT_FRAGMENTRFIDREADINGBOTTOMSHEET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_state_list_bottom_sheet, LAYOUT_FRAGMENTSTATELISTBOTTOMSHEET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_state_multi_list_bottom_sheet, LAYOUT_FRAGMENTSTATEMULTILISTBOTTOMSHEET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_status_multi_list_bottom_sheet, LAYOUT_FRAGMENTSTATUSMULTILISTBOTTOMSHEET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_status_range_bottom_sheet, LAYOUT_FRAGMENTSTATUSRANGEBOTTOMSHEET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_supervisor_mode, LAYOUT_FRAGMENTSUPERVISORMODE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_surveying_first, LAYOUT_FRAGMENTSURVEYINGFIRST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_surveying_second, LAYOUT_FRAGMENTSURVEYINGSECOND);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_transplantation_first, LAYOUT_FRAGMENTTRANSPLANTATIONFIRST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_transplantation_second, LAYOUT_FRAGMENTTRANSPLANTATIONSECOND);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.treeambulance.service.R.layout.fragment_village_multi_list_bottom_sheet, LAYOUT_FRAGMENTVILLAGEMULTILISTBOTTOMSHEET);
  }

  private final ViewDataBinding internalGetViewDataBinding0(DataBindingComponent component,
      View view, int internalId, Object tag) {
    switch(internalId) {
      case  LAYOUT_ACTIVITYDATARETRIEVAL: {
        if ("layout/activity_data_retrieval_0".equals(tag)) {
          return new ActivityDataRetrievalBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_data_retrieval is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYHOME: {
        if ("layout/activity_home_0".equals(tag)) {
          return new ActivityHomeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_home is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYPLANTATIONRETRIEVAL: {
        if ("layout/activity_plantation_retrieval_0".equals(tag)) {
          return new ActivityPlantationRetrievalBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_plantation_retrieval is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYSELECTCOMPANYLIST: {
        if ("layout/activity_select_company_list_0".equals(tag)) {
          return new ActivitySelectCompanyListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_select_company_list is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYSPLASH: {
        if ("layout/activity_splash_0".equals(tag)) {
          return new ActivitySplashBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_splash is invalid. Received: " + tag);
      }
      case  LAYOUT_ADAPTERALLPROJECT: {
        if ("layout/adapter_all_project_0".equals(tag)) {
          return new AdapterAllProjectBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for adapter_all_project is invalid. Received: " + tag);
      }
      case  LAYOUT_ADAPTERBLUETOOTHDEVICE: {
        if ("layout/adapter_bluetooth_device_0".equals(tag)) {
          return new AdapterBluetoothDeviceBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for adapter_bluetooth_device is invalid. Received: " + tag);
      }
      case  LAYOUT_ADAPTERCOMPANYLIST: {
        if ("layout/adapter_company_list_0".equals(tag)) {
          return new AdapterCompanyListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for adapter_company_list is invalid. Received: " + tag);
      }
      case  LAYOUT_ADAPTERCOUNTRYLIST: {
        if ("layout/adapter_country_list_0".equals(tag)) {
          return new AdapterCountryListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for adapter_country_list is invalid. Received: " + tag);
      }
      case  LAYOUT_ADAPTERCURRENCYNAME: {
        if ("layout/adapter_currency_name_0".equals(tag)) {
          return new AdapterCurrencyNameBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for adapter_currency_name is invalid. Received: " + tag);
      }
      case  LAYOUT_ADAPTEREDITSELECTEDBLUETOOTHDEVICE: {
        if ("layout/adapter_edit_selected_bluetooth_device_0".equals(tag)) {
          return new AdapterEditSelectedBluetoothDeviceBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for adapter_edit_selected_bluetooth_device is invalid. Received: " + tag);
      }
      case  LAYOUT_ADAPTERLIST: {
        if ("layout/adapter_list_0".equals(tag)) {
          return new AdapterListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for adapter_list is invalid. Received: " + tag);
      }
      case  LAYOUT_ADAPTERMULTILIST: {
        if ("layout/adapter_multi_list_0".equals(tag)) {
          return new AdapterMultiListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for adapter_multi_list is invalid. Received: " + tag);
      }
      case  LAYOUT_ADAPTERMYORDERSLIST: {
        if ("layout/adapter_my_orders_list_0".equals(tag)) {
          return new AdapterMyOrdersListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for adapter_my_orders_list is invalid. Received: " + tag);
      }
      case  LAYOUT_ADAPTEROPERATORLIST: {
        if ("layout/adapter_operator_list_0".equals(tag)) {
          return new AdapterOperatorListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for adapter_operator_list is invalid. Received: " + tag);
      }
      case  LAYOUT_ADAPTEROPERATORSLIST: {
        if ("layout/adapter_operators_list_0".equals(tag)) {
          return new AdapterOperatorsListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for adapter_operators_list is invalid. Received: " + tag);
      }
      case  LAYOUT_ADAPTERPLANTATIONLIST: {
        if ("layout/adapter_plantation_list_0".equals(tag)) {
          return new AdapterPlantationListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for adapter_plantation_list is invalid. Received: " + tag);
      }
      case  LAYOUT_ADAPTERRANGELIST: {
        if ("layout/adapter_range_list_0".equals(tag)) {
          return new AdapterRangeListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for adapter_range_list is invalid. Received: " + tag);
      }
      case  LAYOUT_ADAPTERREPORTTYPE: {
        if ("layout/adapter_report_type_0".equals(tag)) {
          return new AdapterReportTypeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for adapter_report_type is invalid. Received: " + tag);
      }
      case  LAYOUT_ADAPTERRFIDLIST: {
        if ("layout/adapter_rfid_list_0".equals(tag)) {
          return new AdapterRfidListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for adapter_rfid_list is invalid. Received: " + tag);
      }
      case  LAYOUT_ADAPTERSELECTEDBLUETOOTHDEVICE: {
        if ("layout/adapter_selected_bluetooth_device_0".equals(tag)) {
          return new AdapterSelectedBluetoothDeviceBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for adapter_selected_bluetooth_device is invalid. Received: " + tag);
      }
      case  LAYOUT_ADAPTERSTATELIST: {
        if ("layout/adapter_state_list_0".equals(tag)) {
          return new AdapterStateListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for adapter_state_list is invalid. Received: " + tag);
      }
      case  LAYOUT_ADAPTERSTATUSRANGE: {
        if ("layout/adapter_status_range_0".equals(tag)) {
          return new AdapterStatusRangeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for adapter_status_range is invalid. Received: " + tag);
      }
      case  LAYOUT_ADAPTERTREELIST: {
        if ("layout/adapter_tree_list_0".equals(tag)) {
          return new AdapterTreeListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for adapter_tree_list is invalid. Received: " + tag);
      }
      case  LAYOUT_APPBARMAIN: {
        if ("layout/app_bar_main_0".equals(tag)) {
          return new AppBarMainBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for app_bar_main is invalid. Received: " + tag);
      }
      case  LAYOUT_DUMMY: {
        if ("layout/dummy_0".equals(tag)) {
          return new DummyBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for dummy is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTADDPLANTATION: {
        if ("layout/fragment_add_plantation_0".equals(tag)) {
          return new FragmentAddPlantationBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_add_plantation is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTADDPROJECT: {
        if ("layout/fragment_add_project_0".equals(tag)) {
          return new FragmentAddProjectBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_add_project is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTADDTREEMANAGEMENT: {
        if ("layout/fragment_add_tree_management_0".equals(tag)) {
          return new FragmentAddTreeManagementBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_add_tree_management is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTALLPROJECTS: {
        if ("layout/fragment_all_projects_0".equals(tag)) {
          return new FragmentAllProjectsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_all_projects is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTBLUETOOTHDEVICEBOTTOMSHEET: {
        if ("layout/fragment_bluetooth_device_bottom_sheet_0".equals(tag)) {
          return new FragmentBluetoothDeviceBottomSheetBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_bluetooth_device_bottom_sheet is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCHANGEPASSWORDBOTTOMSHEET: {
        if ("layout/fragment_change_password_bottom_sheet_0".equals(tag)) {
          return new FragmentChangePasswordBottomSheetBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_change_password_bottom_sheet is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCITYMULTILISTBOTTOMSHEET: {
        if ("layout/fragment_city_multi_list_bottom_sheet_0".equals(tag)) {
          return new FragmentCityMultiListBottomSheetBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_city_multi_list_bottom_sheet is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCOUNTRYLISTBOTTOMSHEET: {
        if ("layout/fragment_country_list_bottom_sheet_0".equals(tag)) {
          return new FragmentCountryListBottomSheetBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_country_list_bottom_sheet is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCREATECOMPANY: {
        if ("layout/fragment_create_company_0".equals(tag)) {
          return new FragmentCreateCompanyBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_create_company is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCURRENCYNAMEBOTTOMSHEET: {
        if ("layout/fragment_currency_name_bottom_sheet_0".equals(tag)) {
          return new FragmentCurrencyNameBottomSheetBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_currency_name_bottom_sheet is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCUSTOMDIALOGBOTTOMSHEET: {
        if ("layout/fragment_custom_dialog_bottom_sheet_0".equals(tag)) {
          return new FragmentCustomDialogBottomSheetBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_custom_dialog_bottom_sheet is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTDATARETRIEVAL: {
        if ("layout/fragment_data_retrieval_0".equals(tag)) {
          return new FragmentDataRetrievalBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_data_retrieval is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTDUMMY: {
        if ("layout/fragment_dummy_0".equals(tag)) {
          return new FragmentDummyBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_dummy is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTDUROTEKMODE: {
        if ("layout/fragment_duro_tek_mode_0".equals(tag)) {
          return new FragmentDuroTekModeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_duro_tek_mode is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTEDITCOMPANY: {
        if ("layout/fragment_edit_company_0".equals(tag)) {
          return new FragmentEditCompanyBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_edit_company is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTGPSREADINGBOTTOMSHEET: {
        if ("layout/fragment_gps_reading_bottom_sheet_0".equals(tag)) {
          return new FragmentGpsReadingBottomSheetBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_gps_reading_bottom_sheet is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTLOGINPASSWORDBOTTOMSHEET: {
        if ("layout/fragment_login_password_bottom_sheet_0".equals(tag)) {
          return new FragmentLoginPasswordBottomSheetBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_login_password_bottom_sheet is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTMAINTENANCE: {
        if ("layout/fragment_maintenance_0".equals(tag)) {
          return new FragmentMaintenanceBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_maintenance is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTMANAGEOPERATOR: {
        if ("layout/fragment_manage_operator_0".equals(tag)) {
          return new FragmentManageOperatorBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_manage_operator is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTMODESBOTTOMSHEET: {
        if ("layout/fragment_modes_bottom_sheet_0".equals(tag)) {
          return new FragmentModesBottomSheetBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_modes_bottom_sheet is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTOPERATORLISTBOTTOMSHEET: {
        if ("layout/fragment_operator_list_bottom_sheet_0".equals(tag)) {
          return new FragmentOperatorListBottomSheetBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_operator_list_bottom_sheet is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTOPERATORMULTILISTBOTTOMSHEET: {
        if ("layout/fragment_operator_multi_list_bottom_sheet_0".equals(tag)) {
          return new FragmentOperatorMultiListBottomSheetBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_operator_multi_list_bottom_sheet is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPLANTATION: {
        if ("layout/fragment_plantation_0".equals(tag)) {
          return new FragmentPlantationBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_plantation is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPLANTATIONMAINTENANCE: {
        if ("layout/fragment_plantation_maintenance_0".equals(tag)) {
          return new FragmentPlantationMaintenanceBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_plantation_maintenance is invalid. Received: " + tag);
      }
    }
    return null;
  }

  private final ViewDataBinding internalGetViewDataBinding1(DataBindingComponent component,
      View view, int internalId, Object tag) {
    switch(internalId) {
      case  LAYOUT_FRAGMENTPLANTATIONMAPVIEW: {
        if ("layout/fragment_plantation_map_view_0".equals(tag)) {
          return new FragmentPlantationMapViewBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_plantation_map_view is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPLANTATIONRETRIEVAL: {
        if ("layout/fragment_plantation_retrieval_0".equals(tag)) {
          return new FragmentPlantationRetrievalBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_plantation_retrieval is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPLANTATIONSECOND: {
        if ("layout/fragment_plantation_second_0".equals(tag)) {
          return new FragmentPlantationSecondBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_plantation_second is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPROJECTHOME: {
        if ("layout/fragment_project_home_0".equals(tag)) {
          return new FragmentProjectHomeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_project_home is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPROJECTMAPVIEW: {
        if ("layout/fragment_project_map_view_0".equals(tag)) {
          return new FragmentProjectMapViewBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_project_map_view is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTRANGELISTBOTTOMSHEET: {
        if ("layout/fragment_range_list_bottom_sheet_0".equals(tag)) {
          return new FragmentRangeListBottomSheetBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_range_list_bottom_sheet is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTREPORT: {
        if ("layout/fragment_report_0".equals(tag)) {
          return new FragmentReportBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_report is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTREPORTGENERATION: {
        if ("layout/fragment_report_generation_0".equals(tag)) {
          return new FragmentReportGenerationBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_report_generation is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTREPORTTYPEBOTTOMSHEET: {
        if ("layout/fragment_report_type_bottom_sheet_0".equals(tag)) {
          return new FragmentReportTypeBottomSheetBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_report_type_bottom_sheet is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTREPORTSBOTTOMSHEET: {
        if ("layout/fragment_reports_bottom_sheet_0".equals(tag)) {
          return new FragmentReportsBottomSheetBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_reports_bottom_sheet is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTRFIDREADINGBOTTOMSHEET: {
        if ("layout/fragment_rfid_reading_bottom_sheet_0".equals(tag)) {
          return new FragmentRfidReadingBottomSheetBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_rfid_reading_bottom_sheet is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSTATELISTBOTTOMSHEET: {
        if ("layout/fragment_state_list_bottom_sheet_0".equals(tag)) {
          return new FragmentStateListBottomSheetBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_state_list_bottom_sheet is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSTATEMULTILISTBOTTOMSHEET: {
        if ("layout/fragment_state_multi_list_bottom_sheet_0".equals(tag)) {
          return new FragmentStateMultiListBottomSheetBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_state_multi_list_bottom_sheet is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSTATUSMULTILISTBOTTOMSHEET: {
        if ("layout/fragment_status_multi_list_bottom_sheet_0".equals(tag)) {
          return new FragmentStatusMultiListBottomSheetBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_status_multi_list_bottom_sheet is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSTATUSRANGEBOTTOMSHEET: {
        if ("layout/fragment_status_range_bottom_sheet_0".equals(tag)) {
          return new FragmentStatusRangeBottomSheetBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_status_range_bottom_sheet is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSUPERVISORMODE: {
        if ("layout/fragment_supervisor_mode_0".equals(tag)) {
          return new FragmentSupervisorModeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_supervisor_mode is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSURVEYINGFIRST: {
        if ("layout/fragment_surveying_first_0".equals(tag)) {
          return new FragmentSurveyingFirstBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_surveying_first is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSURVEYINGSECOND: {
        if ("layout/fragment_surveying_second_0".equals(tag)) {
          return new FragmentSurveyingSecondBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_surveying_second is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTTRANSPLANTATIONFIRST: {
        if ("layout/fragment_transplantation_first_0".equals(tag)) {
          return new FragmentTransplantationFirstBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_transplantation_first is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTTRANSPLANTATIONSECOND: {
        if ("layout/fragment_transplantation_second_0".equals(tag)) {
          return new FragmentTransplantationSecondBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_transplantation_second is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTVILLAGEMULTILISTBOTTOMSHEET: {
        if ("layout/fragment_village_multi_list_bottom_sheet_0".equals(tag)) {
          return new FragmentVillageMultiListBottomSheetBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_village_multi_list_bottom_sheet is invalid. Received: " + tag);
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      // find which method will have it. -1 is necessary becausefirst id starts with 1;
      int methodIndex = (localizedLayoutId - 1) / 50;
      switch(methodIndex) {
        case 0: {
          return internalGetViewDataBinding0(component, view, localizedLayoutId, tag);
        }
        case 1: {
          return internalGetViewDataBinding1(component, view, localizedLayoutId, tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(8);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "clickListener");
      sKeys.put(2, "currencyNameModel");
      sKeys.put(3, "model");
      sKeys.put(4, "reportTypeModel");
      sKeys.put(5, "stateCityModel");
      sKeys.put(6, "statusRangeModel");
      sKeys.put(7, "strCompanyName");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(71);

    static {
      sKeys.put("layout/activity_data_retrieval_0", com.treeambulance.service.R.layout.activity_data_retrieval);
      sKeys.put("layout/activity_home_0", com.treeambulance.service.R.layout.activity_home);
      sKeys.put("layout/activity_plantation_retrieval_0", com.treeambulance.service.R.layout.activity_plantation_retrieval);
      sKeys.put("layout/activity_select_company_list_0", com.treeambulance.service.R.layout.activity_select_company_list);
      sKeys.put("layout/activity_splash_0", com.treeambulance.service.R.layout.activity_splash);
      sKeys.put("layout/adapter_all_project_0", com.treeambulance.service.R.layout.adapter_all_project);
      sKeys.put("layout/adapter_bluetooth_device_0", com.treeambulance.service.R.layout.adapter_bluetooth_device);
      sKeys.put("layout/adapter_company_list_0", com.treeambulance.service.R.layout.adapter_company_list);
      sKeys.put("layout/adapter_country_list_0", com.treeambulance.service.R.layout.adapter_country_list);
      sKeys.put("layout/adapter_currency_name_0", com.treeambulance.service.R.layout.adapter_currency_name);
      sKeys.put("layout/adapter_edit_selected_bluetooth_device_0", com.treeambulance.service.R.layout.adapter_edit_selected_bluetooth_device);
      sKeys.put("layout/adapter_list_0", com.treeambulance.service.R.layout.adapter_list);
      sKeys.put("layout/adapter_multi_list_0", com.treeambulance.service.R.layout.adapter_multi_list);
      sKeys.put("layout/adapter_my_orders_list_0", com.treeambulance.service.R.layout.adapter_my_orders_list);
      sKeys.put("layout/adapter_operator_list_0", com.treeambulance.service.R.layout.adapter_operator_list);
      sKeys.put("layout/adapter_operators_list_0", com.treeambulance.service.R.layout.adapter_operators_list);
      sKeys.put("layout/adapter_plantation_list_0", com.treeambulance.service.R.layout.adapter_plantation_list);
      sKeys.put("layout/adapter_range_list_0", com.treeambulance.service.R.layout.adapter_range_list);
      sKeys.put("layout/adapter_report_type_0", com.treeambulance.service.R.layout.adapter_report_type);
      sKeys.put("layout/adapter_rfid_list_0", com.treeambulance.service.R.layout.adapter_rfid_list);
      sKeys.put("layout/adapter_selected_bluetooth_device_0", com.treeambulance.service.R.layout.adapter_selected_bluetooth_device);
      sKeys.put("layout/adapter_state_list_0", com.treeambulance.service.R.layout.adapter_state_list);
      sKeys.put("layout/adapter_status_range_0", com.treeambulance.service.R.layout.adapter_status_range);
      sKeys.put("layout/adapter_tree_list_0", com.treeambulance.service.R.layout.adapter_tree_list);
      sKeys.put("layout/app_bar_main_0", com.treeambulance.service.R.layout.app_bar_main);
      sKeys.put("layout/dummy_0", com.treeambulance.service.R.layout.dummy);
      sKeys.put("layout/fragment_add_plantation_0", com.treeambulance.service.R.layout.fragment_add_plantation);
      sKeys.put("layout/fragment_add_project_0", com.treeambulance.service.R.layout.fragment_add_project);
      sKeys.put("layout/fragment_add_tree_management_0", com.treeambulance.service.R.layout.fragment_add_tree_management);
      sKeys.put("layout/fragment_all_projects_0", com.treeambulance.service.R.layout.fragment_all_projects);
      sKeys.put("layout/fragment_bluetooth_device_bottom_sheet_0", com.treeambulance.service.R.layout.fragment_bluetooth_device_bottom_sheet);
      sKeys.put("layout/fragment_change_password_bottom_sheet_0", com.treeambulance.service.R.layout.fragment_change_password_bottom_sheet);
      sKeys.put("layout/fragment_city_multi_list_bottom_sheet_0", com.treeambulance.service.R.layout.fragment_city_multi_list_bottom_sheet);
      sKeys.put("layout/fragment_country_list_bottom_sheet_0", com.treeambulance.service.R.layout.fragment_country_list_bottom_sheet);
      sKeys.put("layout/fragment_create_company_0", com.treeambulance.service.R.layout.fragment_create_company);
      sKeys.put("layout/fragment_currency_name_bottom_sheet_0", com.treeambulance.service.R.layout.fragment_currency_name_bottom_sheet);
      sKeys.put("layout/fragment_custom_dialog_bottom_sheet_0", com.treeambulance.service.R.layout.fragment_custom_dialog_bottom_sheet);
      sKeys.put("layout/fragment_data_retrieval_0", com.treeambulance.service.R.layout.fragment_data_retrieval);
      sKeys.put("layout/fragment_dummy_0", com.treeambulance.service.R.layout.fragment_dummy);
      sKeys.put("layout/fragment_duro_tek_mode_0", com.treeambulance.service.R.layout.fragment_duro_tek_mode);
      sKeys.put("layout/fragment_edit_company_0", com.treeambulance.service.R.layout.fragment_edit_company);
      sKeys.put("layout/fragment_gps_reading_bottom_sheet_0", com.treeambulance.service.R.layout.fragment_gps_reading_bottom_sheet);
      sKeys.put("layout/fragment_login_password_bottom_sheet_0", com.treeambulance.service.R.layout.fragment_login_password_bottom_sheet);
      sKeys.put("layout/fragment_maintenance_0", com.treeambulance.service.R.layout.fragment_maintenance);
      sKeys.put("layout/fragment_manage_operator_0", com.treeambulance.service.R.layout.fragment_manage_operator);
      sKeys.put("layout/fragment_modes_bottom_sheet_0", com.treeambulance.service.R.layout.fragment_modes_bottom_sheet);
      sKeys.put("layout/fragment_operator_list_bottom_sheet_0", com.treeambulance.service.R.layout.fragment_operator_list_bottom_sheet);
      sKeys.put("layout/fragment_operator_multi_list_bottom_sheet_0", com.treeambulance.service.R.layout.fragment_operator_multi_list_bottom_sheet);
      sKeys.put("layout/fragment_plantation_0", com.treeambulance.service.R.layout.fragment_plantation);
      sKeys.put("layout/fragment_plantation_maintenance_0", com.treeambulance.service.R.layout.fragment_plantation_maintenance);
      sKeys.put("layout/fragment_plantation_map_view_0", com.treeambulance.service.R.layout.fragment_plantation_map_view);
      sKeys.put("layout/fragment_plantation_retrieval_0", com.treeambulance.service.R.layout.fragment_plantation_retrieval);
      sKeys.put("layout/fragment_plantation_second_0", com.treeambulance.service.R.layout.fragment_plantation_second);
      sKeys.put("layout/fragment_project_home_0", com.treeambulance.service.R.layout.fragment_project_home);
      sKeys.put("layout/fragment_project_map_view_0", com.treeambulance.service.R.layout.fragment_project_map_view);
      sKeys.put("layout/fragment_range_list_bottom_sheet_0", com.treeambulance.service.R.layout.fragment_range_list_bottom_sheet);
      sKeys.put("layout/fragment_report_0", com.treeambulance.service.R.layout.fragment_report);
      sKeys.put("layout/fragment_report_generation_0", com.treeambulance.service.R.layout.fragment_report_generation);
      sKeys.put("layout/fragment_report_type_bottom_sheet_0", com.treeambulance.service.R.layout.fragment_report_type_bottom_sheet);
      sKeys.put("layout/fragment_reports_bottom_sheet_0", com.treeambulance.service.R.layout.fragment_reports_bottom_sheet);
      sKeys.put("layout/fragment_rfid_reading_bottom_sheet_0", com.treeambulance.service.R.layout.fragment_rfid_reading_bottom_sheet);
      sKeys.put("layout/fragment_state_list_bottom_sheet_0", com.treeambulance.service.R.layout.fragment_state_list_bottom_sheet);
      sKeys.put("layout/fragment_state_multi_list_bottom_sheet_0", com.treeambulance.service.R.layout.fragment_state_multi_list_bottom_sheet);
      sKeys.put("layout/fragment_status_multi_list_bottom_sheet_0", com.treeambulance.service.R.layout.fragment_status_multi_list_bottom_sheet);
      sKeys.put("layout/fragment_status_range_bottom_sheet_0", com.treeambulance.service.R.layout.fragment_status_range_bottom_sheet);
      sKeys.put("layout/fragment_supervisor_mode_0", com.treeambulance.service.R.layout.fragment_supervisor_mode);
      sKeys.put("layout/fragment_surveying_first_0", com.treeambulance.service.R.layout.fragment_surveying_first);
      sKeys.put("layout/fragment_surveying_second_0", com.treeambulance.service.R.layout.fragment_surveying_second);
      sKeys.put("layout/fragment_transplantation_first_0", com.treeambulance.service.R.layout.fragment_transplantation_first);
      sKeys.put("layout/fragment_transplantation_second_0", com.treeambulance.service.R.layout.fragment_transplantation_second);
      sKeys.put("layout/fragment_village_multi_list_bottom_sheet_0", com.treeambulance.service.R.layout.fragment_village_multi_list_bottom_sheet);
    }
  }
}
