package com.treeambulance.service.databinding;
import com.treeambulance.service.R;
import com.treeambulance.service.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentDummyBindingImpl extends FragmentDummyBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.rlToolBar, 6);
        sViewsWithIds.put(R.id.tvTitle, 7);
        sViewsWithIds.put(R.id.llCompanyName, 8);
        sViewsWithIds.put(R.id.llRFIDName, 9);
        sViewsWithIds.put(R.id.llRFIDList, 10);
        sViewsWithIds.put(R.id.tvRFIDDevicesTitle, 11);
        sViewsWithIds.put(R.id.tvNoOfRFIDDevice, 12);
        sViewsWithIds.put(R.id.rvRFIDDevices, 13);
        sViewsWithIds.put(R.id.tvNoRFID, 14);
        sViewsWithIds.put(R.id.llGPSName, 15);
        sViewsWithIds.put(R.id.llGPSDevicesTitle, 16);
        sViewsWithIds.put(R.id.tvGPSDevicesTitle, 17);
        sViewsWithIds.put(R.id.tvNoOfGPSDevice, 18);
        sViewsWithIds.put(R.id.llGPSList, 19);
        sViewsWithIds.put(R.id.rvGPSDevices, 20);
        sViewsWithIds.put(R.id.tvNoGPS, 21);
    }
    // views
    @NonNull
    private final android.widget.FrameLayout mboundView0;
    // variables
    // values
    // listeners
    private OnClickListenerImpl mClickListenerOnClickAndroidViewViewOnClickListener;
    // Inverse Binding Event Handlers

    public FragmentDummyBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 22, sIncludes, sViewsWithIds));
    }
    private FragmentDummyBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (com.google.android.material.button.MaterialButton) bindings[5]
            , (android.widget.EditText) bindings[4]
            , (android.widget.EditText) bindings[3]
            , (android.widget.EditText) bindings[2]
            , (android.widget.ImageView) bindings[1]
            , (android.widget.LinearLayout) bindings[8]
            , (android.widget.LinearLayout) bindings[16]
            , (android.widget.LinearLayout) bindings[19]
            , (android.widget.LinearLayout) bindings[15]
            , (android.widget.LinearLayout) bindings[10]
            , (android.widget.LinearLayout) bindings[9]
            , (android.widget.RelativeLayout) bindings[6]
            , (androidx.recyclerview.widget.RecyclerView) bindings[20]
            , (androidx.recyclerview.widget.RecyclerView) bindings[13]
            , (android.widget.TextView) bindings[17]
            , (android.widget.TextView) bindings[21]
            , (android.widget.TextView) bindings[18]
            , (android.widget.TextView) bindings[12]
            , (android.widget.TextView) bindings[14]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[7]
            );
        this.btSave.setTag(null);
        this.etAddGPSDevice.setTag(null);
        this.etAddRFIDDevice.setTag(null);
        this.etCompanyName.setTag(null);
        this.ivBackEditCompany.setTag(null);
        this.mboundView0 = (android.widget.FrameLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.clickListener == variableId) {
            setClickListener((android.view.View.OnClickListener) variable);
        }
        else if (BR.strCompanyName == variableId) {
            setStrCompanyName((java.lang.String) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setClickListener(@Nullable android.view.View.OnClickListener ClickListener) {
        this.mClickListener = ClickListener;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.clickListener);
        super.requestRebind();
    }
    public void setStrCompanyName(@Nullable java.lang.String StrCompanyName) {
        this.mStrCompanyName = StrCompanyName;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.strCompanyName);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        android.view.View.OnClickListener clickListenerOnClickAndroidViewViewOnClickListener = null;
        android.view.View.OnClickListener clickListener = mClickListener;
        java.lang.String strCompanyName = mStrCompanyName;

        if ((dirtyFlags & 0x5L) != 0) {



                if (clickListener != null) {
                    // read clickListener::onClick
                    clickListenerOnClickAndroidViewViewOnClickListener = (((mClickListenerOnClickAndroidViewViewOnClickListener == null) ? (mClickListenerOnClickAndroidViewViewOnClickListener = new OnClickListenerImpl()) : mClickListenerOnClickAndroidViewViewOnClickListener).setValue(clickListener));
                }
        }
        if ((dirtyFlags & 0x6L) != 0) {
        }
        // batch finished
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            this.btSave.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etAddGPSDevice.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etAddRFIDDevice.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.etCompanyName.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
            this.ivBackEditCompany.setOnClickListener(clickListenerOnClickAndroidViewViewOnClickListener);
        }
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.etCompanyName, strCompanyName);
        }
    }
    // Listener Stub Implementations
    public static class OnClickListenerImpl implements android.view.View.OnClickListener{
        private android.view.View.OnClickListener value;
        public OnClickListenerImpl setValue(android.view.View.OnClickListener value) {
            this.value = value;
            return value == null ? null : this;
        }
        @Override
        public void onClick(android.view.View arg0) {
            this.value.onClick(arg0); 
        }
    }
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): clickListener
        flag 1 (0x2L): strCompanyName
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}