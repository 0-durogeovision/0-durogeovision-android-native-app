package com.treeambulance.service.restapi;

import com.treeambulance.service.model.*;

import java.util.HashMap;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface ApiInterface {

    /*@GET("geocode/json")
    Flowable<GeoCoderModel> getGeoCoder(@Query("latlng") String latlng,
                                        @Query("key") String apikey);

    @FormUrlEncoded
    @POST("app_api.php")
    Call<stripe_model> striperequest(@FieldMap HashMap<String, String> login);

    @POST("app_api.php")
    Call<stripe_model> stripedetails();

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<CateringListModel> getCateringList(@FieldMap HashMap<String, String> hashMap);

    @POST("app_api.php")
    Observable<CommonModel> getBalancePartialPayment();

    @FormUrlEncoded
    @POST("GetRSA.php")
    Call<ResponseBody> getRSA(@Field("access_code") String access_code, @Field("order_id") String order_id);*/

    /*action:add_projects
      state:Tamil nadu
      district:chennai
      village:tambaram*/

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<CommonModel> adminLogin(@FieldMap HashMap<String, String> hashMap);

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<AddProjectsModel> getAddEditProjects(@FieldMap HashMap<String, String> hashMap);

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<CommonModel> getDeleteProjects(@FieldMap HashMap<String, String> hashMap);

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<ListPlantationModel> getListPlantation(@FieldMap HashMap<String, String> hashMap);

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<ListProjectsModel> getListProjects(@FieldMap HashMap<String, String> hashMap);

    @Multipart
    @POST("app_api.php?")
    Observable<CommonModel> getCreateSurveying(
            @PartMap HashMap<String, RequestBody> data,
            @Part MultipartBody.Part tree1Photo,
            @Part MultipartBody.Part tree2Photo,
            @Part MultipartBody.Part selfiePhoto
    );

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<SurveyDetailsModel> getSurveyDetailsProjects(@FieldMap HashMap<String, String> hashMap);

    @Multipart
    @POST("app_api.php?")
    Observable<CommonModel> getCreateTransplantation(
            @PartMap HashMap<String, RequestBody> data,
            @Part MultipartBody.Part tree1Photo,
            @Part MultipartBody.Part tree2Photo,
            @Part MultipartBody.Part selfiePhoto
    );

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<TreeDetailsModel> getTreeDetails(@FieldMap HashMap<String, String> hashMap);

    @Multipart
    @POST("app_api.php?")
    Observable<CommonModel> getCreateMaintenance(
            @PartMap HashMap<String, RequestBody> data,
            @Part MultipartBody.Part tree1Photo,
            @Part MultipartBody.Part tree2Photo,
            @Part MultipartBody.Part selfiePhoto
    );

    @Multipart
    @POST("app_api.php?")
    Observable<CreateCompanyModel> getCreateCompany(
            @PartMap HashMap<String, RequestBody> data,
            @Part MultipartBody.Part tree1Photo
    );

    @Multipart
    @POST("app_api.php?")
    Observable<EditCompanyModel> getEditCompany(
            @PartMap HashMap<String, RequestBody> data,
            @Part MultipartBody.Part tree1Photo
    );

    @Multipart
    @POST("app_api.php?")
    Observable<EditCompanyModel> getEditCompanyData(
            @PartMap HashMap<String, RequestBody> data
    );



    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<CompanyListModel> getCompanyList(@FieldMap HashMap<String, String> hashMap);

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<CompanyDetailsModel> getCompanyDetails(@FieldMap HashMap<String, String> hashMap);

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<CreateCompanyModel> getCreateOperator(@FieldMap HashMap<String, String> hashMap);

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<OperatorListModel> getOperatorList(@FieldMap HashMap<String, String> hashMap);

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<CountryListModel> getCountryList(@FieldMap HashMap<String, String> hashMap);

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<CommonModel> getDeleteOperator(@FieldMap HashMap<String, String> hashMap);

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<VillageListModel> getVillageList(@FieldMap HashMap<String, String> hashMap);


    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<CommonModel> getSurveyReport(@FieldMap HashMap<String, String> hashMap);

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<CommonModel> getCheckSurveyNo(@FieldMap HashMap<String, String> hashMap);

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<CommonModel> getCheckTreeNo(@FieldMap HashMap<String, String> hashMap);

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<CommonModel> getResetPassword(@FieldMap HashMap<String, String> hashMap);

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<CommonModel> getChangePassword(@FieldMap HashMap<String, String> hashMap);

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<ProjectListDetailsModel> getProjectListDetails(@FieldMap HashMap<String, String> hashMap);

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<SurveyListDetailsModel> getSurveyListDetails(@FieldMap HashMap<String, String> hashMap);

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<CommonModel> getReportGeneration(@FieldMap HashMap<String, String> hashMap);

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<AddTreeManagementModel> getAddEditTreeManagements(@FieldMap HashMap<String, String> hashMap);

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<TreeManagementListModel> getTreeManagementList(@FieldMap HashMap<String, String> hashMap);

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<PlantationListModel> getPlantationProject(@FieldMap HashMap<String, String> hashMap);

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<AddPlantationProjectModel> getAddEditPlantation(@FieldMap HashMap<String, String> hashMap);

    @Multipart
    @POST("app_api.php?")
    Observable<CommonModel> getCreatePlantation(
            @PartMap HashMap<String, RequestBody> data,
            @Part MultipartBody.Part tree1Photo,
            @Part MultipartBody.Part tree2Photo,
            @Part MultipartBody.Part selfiePhoto
    );

    @Multipart
    @POST("app_api.php?")
    Observable<CommonModel> getCreatePlantationMaintenance(
            @PartMap HashMap<String, RequestBody> data,
            @Part MultipartBody.Part tree1Photo,
            @Part MultipartBody.Part tree2Photo,
            @Part MultipartBody.Part selfiePhoto
    );

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<CommonModel> getCheckPlantation(@FieldMap HashMap<String, String> hashMap);

    @FormUrlEncoded
    @POST("app_api.php?")
    Observable<PlantationListModel> getPlantationListDetails(@FieldMap HashMap<String, String> hashMap);

}