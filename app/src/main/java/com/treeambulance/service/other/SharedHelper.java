package com.treeambulance.service.other;

import android.app.Activity;
import android.app.Application;
import android.content.SharedPreferences;

public class SharedHelper {

    private static final String PREF_USER = "USER";
    private static final String PREF_CACHE = "CACHE";
    public static String FCM_TOKEN = "fcm_token";
    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;
    private final Application application;

    public SharedHelper(Application application) {
        this.application = application;
    }

    public void putInUser(String key, String value) {
        preferences = application.getSharedPreferences(PREF_USER, Activity.MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
        editor.commit();
    }

    public String getFromUser(String key) {
        preferences = application.getSharedPreferences(PREF_USER, Activity.MODE_PRIVATE);
        return preferences.getString(key, "");
    }

    public void putInCache(String key, String value) {
        preferences = application.getSharedPreferences(PREF_CACHE, Activity.MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getFromCache(String key) {
        preferences = application.getSharedPreferences(PREF_CACHE, Activity.MODE_PRIVATE);
        return preferences.getString(key, "");
    }

    public void clearCache() {
        preferences = application.getSharedPreferences(PREF_CACHE, Activity.MODE_PRIVATE);
        preferences.edit().clear().apply();
    }

    public void clearUser() {
        preferences = application.getSharedPreferences(PREF_USER, Activity.MODE_PRIVATE);
        preferences.edit().clear().apply();
    }

}