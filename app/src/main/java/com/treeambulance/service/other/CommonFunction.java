package com.treeambulance.service.other;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.tapadoo.alerter.Alerter;
import com.treeambulance.service.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import timber.log.Timber;

public class CommonFunction {

    public final int REQUEST_CODE_MANAGE_EXTERNAL_STORAGE = 2296;
    public final int SPLASH_TIME_OUT = 2000;
    public final int DOWNLOAD_DELAY_TIME = 5000*12;
    protected static KProgressHUD loader = null;
    Application application;

    public CommonFunction(Application application) {
        this.application = application;
    }

    public void showLoader(Activity activity) {
        try {
            if (loader != null) {
                return;
            }

            loader = KProgressHUD.create(activity)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setLabel("Please wait")
                    .setDimAmount(0.5f);
            loader.show();
        } catch (Exception e) {
            Timber.e(e);
        }
    }

    public void dismissLoader() {
        try {
            if (loader != null && loader.isShowing()) {
                loader.dismiss();
                loader = null;
            }
        } catch (Exception e) {
            Timber.e(e);
        }
    }

    public String NullPointer(String value) {
        if (value != null && !value.equalsIgnoreCase("null") && !value.equalsIgnoreCase("") && !value.isEmpty()) {
            return value;
        } else {
            return "";
        }
    }

    public Boolean NullPointerValidator(String string) {
        if (string != null && !string.equalsIgnoreCase("") && !string.isEmpty() && !string.equalsIgnoreCase("null")) {
            return true;
        } else {
            return false;
        }
    }

    public String getJsonDataFromAsset(Context context, String fileName) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public String getReverseDate(String dateString) {
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            Date date = fmt.parse(dateString);
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd-MMM-yyyy");
            return fmtOut.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateString;
    }

    public String[] getCurrentDate() {

        Date date = Calendar.getInstance().getTime();

        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        return new String[]{simpleDateFormat1.format(date), simpleDateFormat2.format(date)};
    }

    public Uri getImageUri(Context context, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        inImage.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
//        String path =' MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), inImage, "IMG_" + Calendar.getInstance().getTime(), null);
        return Uri.parse(path);
    }

    public Bitmap getResizedBitmap(Bitmap image) {
        int maxSize = 360;  //450, 1000
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public String getRealPathFromURI(Activity activity, Uri uri) {
        Cursor cursor = activity.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public MultipartBody.Part getMultipartBody(String param, String path) {
        File file = null;
        MultipartBody.Part fileToUpload = null;
        if (path != null) {
            file = new File(path);
            fileToUpload = MultipartBody.Part.createFormData(param, file.getName(), RequestBody.create(file, MediaType.parse("*/*")));
        }
        return fileToUpload;
    }

    public MultipartBody.Part requestImage(String param, String myImagelist, Context context) {

        MultipartBody.Part surveyImagesParts = null;

        String strFilePath = "";
        Timber.i("requestImage: survey image " + myImagelist);
//        strFilePath = compressImage(myImagelist, context);
        strFilePath = myImagelist;
//            File file = new File(myImagelist.get(index));
        File file = new File(strFilePath);
        RequestBody surveyBody = RequestBody.create(file, MediaType.parse("image/*"));
        surveyImagesParts = MultipartBody.Part.createFormData(param,
                file.getName(),
                surveyBody);

        return surveyImagesParts;
    }

    public String getFilename(Context context) {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), context.getResources().getString(R.string.app_name) + "/images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    private String getRealPathFromURI(String contentURI, Context contextCompress) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = contextCompress.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public String compressImage(String imageUri, Context contextCompress) {

        String filePath = getRealPathFromURI(imageUri, contextCompress);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename(contextCompress);
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 50, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    public void showLongToast(String value) {
//        Toast.makeText(application, value, Toast.LENGTH_LONG).show();
        Toast toast = Toast.makeText(application, value, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public void showShortToast(String value) {
//        Toast.makeText(application, value, Toast.LENGTH_LONG).show();
        Toast toast = Toast.makeText(application, value, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public void displayErrorSnackbar(View view, Context context, String message/*, Typeface typeface*/) {
        try {
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/avenir_medium.ttf");
            Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
            snackbar.setAction("Action", null);
            View snackBarView = snackbar.getView();
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) snackBarView.getLayoutParams();
            params.gravity = Gravity.BOTTOM;
            TextView textView = (TextView) (snackBarView.findViewById(com.google.android.material.R.id.snackbar_text));
            textView.setTypeface(typeface);
            snackBarView.setLayoutParams(params);
            snackBarView.setBackgroundColor(ContextCompat.getColor(context, R.color.err_red));
            snackbar.show();

        } catch (Exception e) {
            e.printStackTrace();
            Timber.e("displayErrorSnackbar: %s", e.getMessage());
            Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show();
        }
    }

    public void displaySuccessSnackbar(View view, Context context, String message/*, Typeface typeface*/) {
        try {
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/avenir_medium.ttf");
            Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
            snackbar.setAction("Action", null);
            View snackBarView = snackbar.getView();
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) snackBarView.getLayoutParams();
            params.gravity = Gravity.CENTER;
            TextView textView = (TextView) (snackBarView.findViewById(com.google.android.material.R.id.snackbar_text));
            textView.setTypeface(typeface);
            snackBarView.setLayoutParams(params);
            snackBarView.setBackgroundColor(ContextCompat.getColor(context, R.color.green));
            snackbar.show();

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show();
        }
    }

    public void displayNetworkErrorSnackbar(View view, Context context) {
        try {
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/avenir_medium.ttf");
            Snackbar snackbar = Snackbar.make(view, R.string.connection, Snackbar.LENGTH_LONG);
            snackbar.setAction("Action", null);
            View snackBarView = snackbar.getView();
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) snackBarView.getLayoutParams();
            params.gravity = Gravity.CENTER;
            TextView textView = (TextView) (snackBarView.findViewById(com.google.android.material.R.id.snackbar_text));
            textView.setTypeface(typeface);
            snackBarView.setLayoutParams(params);
            snackBarView.setBackgroundColor(ContextCompat.getColor(context, R.color.light_red));
            snackbar.show();

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, R.string.connection, Toast.LENGTH_SHORT).show();
        }
    }

    public void showNoNetwork(Activity activity/*, Typeface typeface*/) {
        if (activity != null && !activity.isDestroyed()) {

            Alerter.create(activity)
                    .setIcon(R.drawable.ic_no_wifi)
                    .setTitle(R.string.internet_connect)
                    .setText(R.string.connection)
                    .setBackgroundColorRes(R.color.light_red)
                    .enableSwipeToDismiss()
                    .setProgressColorRes(R.color.black)
                    .setTitleTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/avenir_bold.ttf"))
                    .setTextTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/avenir_medium.ttf"))
                    .show();
                    /*.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.setClassName("com.android.phone", "com.android.phone.NetworkSetting");
                            activity.startActivity(intent);
                        }
                    }).show();*/
        }
    }

    public void SuccessAlert(Activity activity, String title, String message/*, Typeface typeface*/) {

        if (activity != null) {
            Alerter.create(activity)
                    .setIcon(R.drawable.ic_check)
                    .setTitle(title)
                    .setDuration(2000)
                    .setTitleAppearance(R.style.AlertTextAppearance_Title)
                    .setText(message)
                    .setTextAppearance(R.style.AlertTextAppearance_Text)
                    .setTitleTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/avenir_bold.ttf"))
                    .setTextTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/avenir_medium.ttf"))
                    .setBackgroundColorRes(R.color.green)
                    .enableSwipeToDismiss()
                    .show();
        }
    }

    public void FailureAlert(Activity activity, String title, String message/*, Typeface typeface*/) {

        if (activity != null && !activity.isDestroyed()) {
            Alerter.create(activity)
                    .setIcon(R.drawable.ic_err_cross)
                    .setTitle(title)
                    .setTitleAppearance(R.style.AlertTextAppearance_Title)
                    .setText(message)
                    .setTextAppearance(R.style.AlertTextAppearance_Text)
                    .setTitleTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/avenir_bold.ttf"))
                    .setTextTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/avenir_medium.ttf"))
                    .setBackgroundColorRes(R.color.err_red)
                    .enableSwipeToDismiss()
                    .show();
        }
    }

    public void WarningAlert(Activity activity, String title, String message/*, Typeface typeface*/) {

        if (activity != null) {
            Alerter.create(activity)
                    .setIcon(R.drawable.ic_warning)
                    .setTitle(title)
                    .setDuration(2000)
                    .setTitleAppearance(R.style.AlertTextAppearance_Title)
                    .setText(message)
                    .setTextAppearance(R.style.AlertTextAppearance_Text)
                    .setTitleTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/avenir_bold.ttf"))
                    .setTextTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/avenir_medium.ttf"))
                    .setBackgroundColorRes(R.color.light_red)
                    .enableSwipeToDismiss()
                    .show();
        }
    }

    public String getDistance(double startLatitude, double startLongitude, double endLatitude, double endLongitude) {
        float distance = 0;

        float[] results = new float[1];

        Location.distanceBetween(startLatitude, startLongitude, endLatitude, endLongitude, results);
        distance = results[0];

        return "" + distance;
    }

    public List<Address> getAddress(Activity activity,double latitude, double longitude){
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(activity, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null && addresses.size() > 0) {
                  return addresses;
//                Address address = addresses.get(0);
//                StringBuilder sb = new StringBuilder();
//                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
//                    sb.append(address.getAddressLine(i)).append("\n");
//                }
//                sb.append(address.getLocality()).append("\n");
//                sb.append(address.getPostalCode()).append("\n");
//                sb.append(address.getCountryName());
//                result = sb.toString();
            } else {
                return addresses;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return addresses;
    }

    public void showSettingsAlert(Context mContext) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                mContext);
        alertDialog.setTitle("SETTINGS");
        alertDialog.setMessage("Enable Location Provider! Go to settings menu?");
        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        mContext.startActivity(intent);
                    }
                });
        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }

    public void showSuccessAlert(Context mContext,String title,String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                mContext,R.style.SuccessAlertDialog);
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(title,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }

    public void showErrorAlert(Context mContext,String title,String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                mContext,R.style.ErrorAlertDialog);
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(title,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }

}
