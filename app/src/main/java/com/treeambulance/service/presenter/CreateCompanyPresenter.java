package com.treeambulance.service.presenter;

import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.model.CreateCompanyModel;
import com.treeambulance.service.other.Utiles;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.treeambulance.service.restapi.ApiClient.getRetrofitInstance;

public class CreateCompanyPresenter {

    private final CompositeDisposable compositeDisposable;
    private ContactInterface contactInterface;

    public CreateCompanyPresenter(ContactInterface contactInterface) {
        this.contactInterface = contactInterface;
        compositeDisposable = new CompositeDisposable();
    }

    public void getCreateCompany(HashMap<String, RequestBody> createSurveyingHashMap,
                                   MultipartBody.Part tree1Photo) {
        compositeDisposable.add(getRetrofitInstance()
                .getCreateCompany(createSurveyingHashMap, tree1Photo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessCreateCompany, this::onErrorCreateCompany));
    }

    private void onSuccessCreateCompany(CreateCompanyModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessCreateCompany(model);
        } else {
            contactInterface.onErrorCreateCompany(model.getMsg());
        }
    }

    private void onErrorCreateCompany(Throwable throwable) {
        contactInterface.onErrorCreateCompany(Utiles.getBodyError(throwable));
    }

    public void onDispose() {
        contactInterface = null;
        if (compositeDisposable != null)
            compositeDisposable.dispose();
    }

    public interface ContactInterface {

        void onSuccessCreateCompany(CreateCompanyModel model);

        void onErrorCreateCompany(String error);
    }
}
