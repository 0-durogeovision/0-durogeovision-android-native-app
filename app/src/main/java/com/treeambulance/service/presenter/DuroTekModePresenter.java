package com.treeambulance.service.presenter;

import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.other.Utiles;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.treeambulance.service.restapi.ApiClient.getRetrofitInstance;

public class DuroTekModePresenter {
    private final CompositeDisposable compositeDisposable;
    private ContactInterface contactInterface;

    public DuroTekModePresenter(ContactInterface contactInterface) {
        this.contactInterface = contactInterface;
        compositeDisposable = new CompositeDisposable();
    }

    public void getResetPassword(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getResetPassword(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessResetPassword, this::onErrorResetPassword));
    }

    private void onSuccessResetPassword(CommonModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessResetPassword(model);
        } else {
            contactInterface.onErrorResetPassword(model.getMsg());
        }
    }

    private void onErrorResetPassword(Throwable throwable) {
        contactInterface.onErrorResetPassword(Utiles.getBodyError(throwable));
    }

    public void onDispose() {
        contactInterface = null;
        if (compositeDisposable != null)
            compositeDisposable.dispose();
    }

    public interface ContactInterface {
        void onSuccessResetPassword(CommonModel commonModel);

        void onErrorResetPassword(String error);
    }
}