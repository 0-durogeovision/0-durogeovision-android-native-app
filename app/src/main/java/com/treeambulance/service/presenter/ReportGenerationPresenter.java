package com.treeambulance.service.presenter;

import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.model.CompanyDetailsModel;
import com.treeambulance.service.model.CountryListModel;
import com.treeambulance.service.model.ListProjectsModel;
import com.treeambulance.service.model.OperatorListModel;
import com.treeambulance.service.model.SurveyDetailsModel;
import com.treeambulance.service.model.VillageListModel;
import com.treeambulance.service.other.Utiles;
import com.treeambulance.service.views.fragment.ReportGenerationFragment;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.treeambulance.service.restapi.ApiClient.getRetrofitInstance;

public class ReportGenerationPresenter {
    private final CompositeDisposable compositeDisposable;
    private ReportGenerationPresenter.ContactInterface contactInterface;

    public ReportGenerationPresenter(ReportGenerationPresenter.ContactInterface contactInterface) {
        this.contactInterface = contactInterface;
        compositeDisposable = new CompositeDisposable();
    }

    public void getOperatorList(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getOperatorList(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessOperatorList, this::onErrorOperatorList));
    }

    public void getCountryList(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getCountryList(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessCountryList, this::onErrorCountryList));
    }

    public void getVillageList(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getVillageList(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessVillageList, this::onErrorVillageList));
    }

    public void getSurveyReport(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getSurveyReport(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessSurveyReport, this::onErrorSurveyReport));
    }

    public void getTransplantationReport(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getSurveyReport(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessTransplantationReport, this::onErrorTransplantationReport));
    }

    public void getMaintenanceReport(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getSurveyReport(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessMaintenanceReport, this::onErrorMaintenanceReport));
    }

    public void getConsolidatedReport(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getSurveyReport(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessConsolidatedReport, this::onErrorConsolidatedReport));
    }

    public void getReportGeneration(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getReportGeneration(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessReportGeneration, this::onErrorReportGeneration));
    }

    private void onSuccessOperatorList(OperatorListModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessOperatorList(model);
        } else {
            contactInterface.onErrorOperatorList(model.getMsg());
        }
    }

    private void onErrorOperatorList(Throwable throwable) {
        contactInterface.onErrorOperatorList(Utiles.getBodyError(throwable));
    }

    private void onSuccessCountryList(CountryListModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessCountryList(model);
        } else {
            contactInterface.onErrorCountryList(model.getMsg());
        }
    }

    private void onErrorCountryList(Throwable throwable) {
        contactInterface.onErrorCountryList(Utiles.getBodyError(throwable));
    }

    private void onSuccessVillageList(VillageListModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessVillageList(model);
        } else {
            contactInterface.onErrorVillageList(model.getMsg());
        }
    }

    private void onErrorVillageList(Throwable throwable) {
        contactInterface.onErrorVillageList(Utiles.getBodyError(throwable));
    }

    private void onSuccessSurveyReport(CommonModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessSurveyReport(model);
        } else {
            contactInterface.onErrorSurveyReport(model.getMsg());
        }
    }

    private void onErrorSurveyReport(Throwable throwable) {
        contactInterface.onErrorSurveyReport(Utiles.getBodyError(throwable));
    }

    private void onSuccessTransplantationReport(CommonModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessTransplantationReport(model);
        } else {
            contactInterface.onErrorTransplantationReport(model.getMsg());
        }
    }

    private void onErrorTransplantationReport(Throwable throwable) {
        contactInterface.onErrorTransplantationReport(Utiles.getBodyError(throwable));
    }

    private void onSuccessMaintenanceReport(CommonModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessMaintenanceReport(model);
        } else {
            contactInterface.onErrorMaintenanceReport(model.getMsg());
        }
    }

    private void onErrorMaintenanceReport(Throwable throwable) {
        contactInterface.onErrorMaintenanceReport(Utiles.getBodyError(throwable));
    }

    private void onSuccessReportGeneration(CommonModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessReportGeneration(model);
        } else {
            contactInterface.onErrorReportGeneration(model.getMsg());
        }
    }

    private void onErrorReportGeneration(Throwable throwable) {
        contactInterface.onErrorReportGeneration(Utiles.getBodyError(throwable));
    }

    private void onSuccessConsolidatedReport(CommonModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessConsolidatedReport(model);
        } else {
            contactInterface.onErrorConsolidatedReport(model.getMsg());
        }
    }

    private void onErrorConsolidatedReport(Throwable throwable) {
        contactInterface.onErrorConsolidatedReport(Utiles.getBodyError(throwable));
    }

    public void onDispose() {
        contactInterface = null;
        if (compositeDisposable != null)
            compositeDisposable.dispose();
    }

    public interface ContactInterface {

        void onSuccessOperatorList(OperatorListModel operatorListModel);

        void onErrorOperatorList(String error);

        void onSuccessCountryList(CountryListModel countryListModel);

        void onErrorCountryList(String error);

        void onSuccessVillageList(VillageListModel villageListModel);

        void onErrorVillageList(String error);

        void onSuccessSurveyReport(CommonModel commonModel);

        void onErrorSurveyReport(String error);

        void onSuccessTransplantationReport(CommonModel commonModel);

        void onErrorTransplantationReport(String error);

        void onSuccessMaintenanceReport(CommonModel commonModel);

        void onErrorMaintenanceReport(String error);

        void onSuccessConsolidatedReport(CommonModel commonModel);

        void onErrorConsolidatedReport(String error);

        void onSuccessReportGeneration(CommonModel commonModel);

        void onErrorReportGeneration(String error);

    }
}
