package com.treeambulance.service.presenter;

import com.treeambulance.service.model.CompanyDetailsModel;
import com.treeambulance.service.model.ListPlantationModel;
import com.treeambulance.service.model.ListProjectsModel;
import com.treeambulance.service.model.PlantationListModel;
import com.treeambulance.service.model.ProjectListDetailsModel;
import com.treeambulance.service.model.SurveyListDetailsModel;
import com.treeambulance.service.other.Utiles;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.treeambulance.service.restapi.ApiClient.getRetrofitInstance;

public class PlantationRetrievalPresenter {
    private final CompositeDisposable compositeDisposable;
    private ContactInterface contactInterface;

    public PlantationRetrievalPresenter(ContactInterface contactInterface) {
        this.contactInterface = contactInterface;
        compositeDisposable = new CompositeDisposable();
    }

    public void getCompanyDetails(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getCompanyDetails(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessCompanyDetails, this::onErrorCompanyDetails));
    }

    public void getListPlantations(HashMap<String, String> loginHashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getListPlantation(loginHashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessListProjects, this::onErrorListProjects));
    }

    public void getProjectListDetails(HashMap<String, String> deleteHashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getPlantationListDetails(deleteHashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessProjectListDetails, this::onErrorProjectListDetails));
    }

    public void getSurveyListDetails(HashMap<String, String> deleteHashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getSurveyListDetails(deleteHashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessSurveyListDetails, this::onErrorSurveyListDetails));
    }

    private void onSuccessCompanyDetails(CompanyDetailsModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessCompanyDetails(model);
        } else {
            contactInterface.onErrorCompanyDetails(model.getMsg());
        }
    }

    private void onErrorCompanyDetails(Throwable throwable) {
        contactInterface.onErrorCompanyDetails(Utiles.getBodyError(throwable));
    }

    private void onSuccessListProjects(ListPlantationModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessListProjects(model);
        } else {
            contactInterface.onErrorListProjects(model.getMsg());
        }
    }

    private void onErrorListProjects(Throwable throwable) {
        contactInterface.onErrorListProjects(Utiles.getBodyError(throwable));
    }

    private void onSuccessProjectListDetails(PlantationListModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessProjectListDetails(model);
        } else {
            contactInterface.onErrorProjectListDetails(model.getMsg());
        }
    }

    private void onErrorProjectListDetails(Throwable throwable) {
        contactInterface.onErrorProjectListDetails(Utiles.getBodyError(throwable));
    }

    private void onSuccessSurveyListDetails(SurveyListDetailsModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessSurveyListDetails(model);
        } else {
            contactInterface.onErrorSurveyListDetails(model.getMsg());
        }
    }

    private void onErrorSurveyListDetails(Throwable throwable) {
        contactInterface.onErrorSurveyListDetails(Utiles.getBodyError(throwable));
    }

    public void onDispose() {
        contactInterface = null;
        if (compositeDisposable != null)
            compositeDisposable.dispose();
    }

    public interface ContactInterface {

        void onSuccessCompanyDetails(CompanyDetailsModel companyDetailsModel);

        void onErrorCompanyDetails(String error);

        void onSuccessListProjects(ListPlantationModel listProjectsModel);

        void onErrorListProjects(String error);

        void onSuccessProjectListDetails(PlantationListModel projectListDetailsModel);

        void onErrorProjectListDetails(String error);

        void onSuccessSurveyListDetails(SurveyListDetailsModel surveyListDetailsModel);

        void onErrorSurveyListDetails(String error);
    }
}
