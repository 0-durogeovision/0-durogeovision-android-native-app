package com.treeambulance.service.presenter;

import io.reactivex.disposables.CompositeDisposable;

public class LoginPresenter {
    private ContactInterface contactInterface;
    private CompositeDisposable compositeDisposable;

    public LoginPresenter(ContactInterface contactInterface) {
        this.contactInterface = contactInterface;
        compositeDisposable = new CompositeDisposable();
    }

    /*public void getLogin(HashMap<String, String> loginHashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getLogin(loginHashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessLogin, this::onErrorLogin));
    }

    private void onSuccessLogin(LoginModel loginModel) {

        if (loginModel.getStatus().equalsIgnoreCase("success")) {

            contactInterface.onSuccessLogin(loginModel);
        } else {
            contactInterface.onErrorLogin(loginModel.getMsg());
        }
    }

    private void onErrorLogin(Throwable throwable) {

        contactInterface.onErrorLogin(Utiles.getBodyError(throwable));
    }*/

    public void onDispose() {
        contactInterface = null;
        if (compositeDisposable != null)
            compositeDisposable.dispose();
    }

    public interface ContactInterface {
//        void onSuccessLogin(LoginModel loginModel);

//        void onErrorLogin(String error);
    }
}
