package com.treeambulance.service.presenter;

import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.other.Utiles;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.treeambulance.service.restapi.ApiClient.getRetrofitInstance;

public class SupervisorModePresenter {
    private final CompositeDisposable compositeDisposable;
    private ContactInterface contactInterface;

    public SupervisorModePresenter(ContactInterface contactInterface) {
        this.contactInterface = contactInterface;
        compositeDisposable = new CompositeDisposable();
    }

    public void getChangePassword(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getChangePassword(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessChangePassword, this::onErrorChangePassword));
    }

    private void onSuccessChangePassword(CommonModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessChangePassword(model);
        } else {
            contactInterface.onErrorChangePassword(model.getMsg());
        }
    }

    private void onErrorChangePassword(Throwable throwable) {
        contactInterface.onErrorChangePassword(Utiles.getBodyError(throwable));
    }

    public void onDispose() {
        contactInterface = null;
        if (compositeDisposable != null)
            compositeDisposable.dispose();
    }

    public interface ContactInterface {
        void onSuccessChangePassword(CommonModel commonModel);

        void onErrorChangePassword(String error);
    }
}