package com.treeambulance.service.presenter;

import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.model.CompanyDetailsModel;
import com.treeambulance.service.model.CreateCompanyModel;
import com.treeambulance.service.other.Utiles;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.treeambulance.service.restapi.ApiClient.getRetrofitInstance;

public class HomePresenter {

    private final CompositeDisposable compositeDisposable;
    private ContactInterface contactInterface;

    public HomePresenter(ContactInterface contactInterface) {
        this.contactInterface = contactInterface;
        compositeDisposable = new CompositeDisposable();
    }

    public void getLoginPassword(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .adminLogin(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessLoginPassword, this::onErrorLoginPassword));
    }

    public void getCompanyDetails(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getCompanyDetails(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessCompanyDetails, this::onErrorCompanyDetails));
    }

    private void onSuccessLoginPassword(CommonModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessLoginPassword(model);
        } else {
            contactInterface.onErrorLoginPassword(model.getMsg());
        }
    }

    private void onErrorLoginPassword(Throwable throwable) {
        contactInterface.onErrorLoginPassword(Utiles.getBodyError(throwable));
    }

    private void onSuccessCompanyDetails(CompanyDetailsModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessCompanyDetails(model);
        } else {
            contactInterface.onErrorCompanyDetails(model.getMsg());
        }
    }

    private void onErrorCompanyDetails(Throwable throwable) {
        contactInterface.onErrorCompanyDetails(Utiles.getBodyError(throwable));
    }

    public void onDispose() {
        contactInterface = null;
        if (compositeDisposable != null)
            compositeDisposable.dispose();
    }

    public interface ContactInterface {

        void onSuccessLoginPassword(CommonModel model);

        void onErrorLoginPassword(String error);

        void onSuccessCompanyDetails(CompanyDetailsModel companyDetailsModel);

        void onErrorCompanyDetails(String error);
    }
}
