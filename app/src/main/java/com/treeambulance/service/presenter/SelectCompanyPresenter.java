package com.treeambulance.service.presenter;

import com.treeambulance.service.model.CompanyListModel;
import com.treeambulance.service.other.Utiles;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.treeambulance.service.restapi.ApiClient.getRetrofitInstance;

public class SelectCompanyPresenter {

    private final CompositeDisposable compositeDisposable;
    private ContactInterface contactInterface;

    public SelectCompanyPresenter(ContactInterface contactInterface) {
        this.contactInterface = contactInterface;
        compositeDisposable = new CompositeDisposable();
    }

    public void getCompanyList(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getCompanyList(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessCompanyList, this::onErrorCompanyList));
    }

    private void onSuccessCompanyList(CompanyListModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessCompanyList(model);
        } else {
            contactInterface.onErrorCompanyList(model.getMsg());
        }
    }

    private void onErrorCompanyList(Throwable throwable) {
        contactInterface.onErrorCompanyList(Utiles.getBodyError(throwable));
    }

    public void onDispose() {
        contactInterface = null;
        if (compositeDisposable != null)
            compositeDisposable.dispose();
    }

    public interface ContactInterface {
        void onSuccessCompanyList(CompanyListModel companyListModel);

        void onErrorCompanyList(String error);
    }
}
