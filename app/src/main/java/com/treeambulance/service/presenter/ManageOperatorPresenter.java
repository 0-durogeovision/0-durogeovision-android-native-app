package com.treeambulance.service.presenter;

import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.model.CreateCompanyModel;
import com.treeambulance.service.model.OperatorListModel;
import com.treeambulance.service.other.Utiles;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.treeambulance.service.restapi.ApiClient.getRetrofitInstance;

public class ManageOperatorPresenter {
    private final CompositeDisposable compositeDisposable;
    private ContactInterface contactInterface;

    public ManageOperatorPresenter(ContactInterface contactInterface) {
        this.contactInterface = contactInterface;
        compositeDisposable = new CompositeDisposable();
    }

    public void getOperatorList(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getOperatorList(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessOperatorList, this::onErrorOperatorList));
    }

    public void getCreateOperator(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getCreateOperator(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessCreateOperator, this::onErrorCreateOperator));
    }

    public void getDeleteOperator(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getDeleteOperator(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessDeleteProjects, this::onErrorDeleteProjects));
    }

    private void onSuccessOperatorList(OperatorListModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessOperatorList(model);
        } else {
            contactInterface.onErrorOperatorList(model.getMsg());
        }
    }

    private void onErrorOperatorList(Throwable throwable) {
        contactInterface.onErrorOperatorList(Utiles.getBodyError(throwable));
    }

    private void onSuccessCreateOperator(CreateCompanyModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessCreateOperator(model);
        } else {
            contactInterface.onErrorCreateOperator(model.getMsg());
        }
    }

    private void onErrorCreateOperator(Throwable throwable) {
        contactInterface.onErrorCreateOperator(Utiles.getBodyError(throwable));
    }

    private void onSuccessDeleteProjects(CommonModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessDeleteProjects(model);
        } else {
            contactInterface.onErrorDeleteProjects(model.getMsg());
        }
    }

    private void onErrorDeleteProjects(Throwable throwable) {
        contactInterface.onErrorDeleteProjects(Utiles.getBodyError(throwable));
    }

    public void onDispose() {
        contactInterface = null;
        if (compositeDisposable != null)
            compositeDisposable.dispose();
    }

    public interface ContactInterface {
        void onSuccessOperatorList(OperatorListModel operatorListModel);

        void onErrorOperatorList(String error);

        void onSuccessCreateOperator(CreateCompanyModel createCompanyModel);

        void onErrorCreateOperator(String error);

        void onSuccessDeleteProjects(CommonModel commonModel);

        void onErrorDeleteProjects(String error);
    }

}
