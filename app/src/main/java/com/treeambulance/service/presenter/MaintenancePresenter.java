package com.treeambulance.service.presenter;

import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.model.ListProjectsModel;
import com.treeambulance.service.model.OperatorListModel;
import com.treeambulance.service.model.TreeDetailsModel;
import com.treeambulance.service.other.Utiles;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.treeambulance.service.restapi.ApiClient.getRetrofitInstance;

public class MaintenancePresenter {
    private final CompositeDisposable compositeDisposable;
    private ContactInterface contactInterface;

    public MaintenancePresenter(ContactInterface contactInterface) {
        this.contactInterface = contactInterface;
        compositeDisposable = new CompositeDisposable();
    }

    public void getListProjects(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getListProjects(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessListProjects, this::onErrorListProjects));
    }

    public void getTreeDetails(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getTreeDetails(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessTreeDetails, this::onErrorTreeDetails));
    }

    public void getOperatorList(HashMap<String, String> hashMap) {
        compositeDisposable.add(getRetrofitInstance()
                .getOperatorList(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessOperatorList, this::onErrorOperatorList));
    }

    public void getCreateMaintenance(HashMap<String, RequestBody> hashMap,
                                     MultipartBody.Part tree1Photo, MultipartBody.Part tree2Photo,
                                     MultipartBody.Part selfiePhoto) {
        compositeDisposable.add(getRetrofitInstance()
                .getCreateMaintenance(hashMap, tree1Photo, tree2Photo, selfiePhoto)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessCreateMaintenance, this::onErrorCreateMaintenance));
    }

    private void onSuccessTreeDetails(TreeDetailsModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessTreeDetails(model);
        } else {
            contactInterface.onErrorTreeDetails(model.getMsg());
        }
    }

    private void onSuccessListProjects(ListProjectsModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessListProjects(model);
        } else {
            contactInterface.onErrorListProjects(model.getMsg());
        }
    }

    private void onSuccessOperatorList(OperatorListModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessOperatorList(model);
        } else {
            contactInterface.onErrorOperatorList(model.getMsg());
        }
    }

    private void onSuccessCreateMaintenance(CommonModel model) {
        if (model.getStatus().equalsIgnoreCase("success")) {
            contactInterface.onSuccessCreateMaintenance(model);
        } else {
            contactInterface.onErrorCreateMaintenance(model.getMsg());
        }
    }

    private void onErrorListProjects(Throwable throwable) {
        contactInterface.onErrorListProjects(Utiles.getBodyError(throwable));
    }

    private void onErrorTreeDetails(Throwable throwable) {
        contactInterface.onErrorTreeDetails(Utiles.getBodyError(throwable));
    }

    private void onErrorOperatorList(Throwable throwable) {
        contactInterface.onErrorOperatorList(Utiles.getBodyError(throwable));
    }

    private void onErrorCreateMaintenance(Throwable throwable) {
        contactInterface.onErrorCreateMaintenance(Utiles.getBodyError(throwable));
    }

    public void onDispose() {
        contactInterface = null;
        if (compositeDisposable != null)
            compositeDisposable.dispose();
    }

    public interface ContactInterface {
        void onSuccessListProjects(ListProjectsModel listProjectsModel);

        void onErrorListProjects(String error);

        void onSuccessTreeDetails(TreeDetailsModel listProjectsModel);

        void onErrorTreeDetails(String error);

        void onSuccessOperatorList(OperatorListModel operatorListModel);

        void onErrorOperatorList(String error);

        void onSuccessCreateMaintenance(CommonModel model);

        void onErrorCreateMaintenance(String error);
    }
}
