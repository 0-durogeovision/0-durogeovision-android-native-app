package com.treeambulance.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddTreeManagementModel {

    @Expose
    @SerializedName("result")
    private Result result;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private String status;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class Result {
        @Expose
        @SerializedName("etLocalTreeName")
        private String etLocalTreeName;
        @Expose
        @SerializedName("etBotanicalTreeName")
        private String etBotanicalTreeName;
        @Expose
        @SerializedName("id")
        private String id;

        public String getEtLocalTreeName() {
            return etLocalTreeName;
        }

        public void setEtLocalTreeName(String etLocalTreeName) {
            this.etLocalTreeName = etLocalTreeName;
        }

        public String getEtBotanicalTreeName() {
            return etBotanicalTreeName;
        }

        public void setEtBotanicalTreeName(String village) {
            this.etBotanicalTreeName = etBotanicalTreeName;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
