package com.treeambulance.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddPlantationProjectModel {

    @Expose
    @SerializedName("result")
    private Result result;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private String status;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class Result {
        @Expose
        @SerializedName("plantation_name")
        private String plantationName;
        @Expose
        @SerializedName("state")
        private String state;
        @Expose
        @SerializedName("country")
        private String country;
        @Expose
        @SerializedName("district")
        private String district;
        @Expose
        @SerializedName("village")
        private String village;
        @Expose
        @SerializedName("location")
        private String location;
        @Expose
        @SerializedName("company_id")
        private String company_id;
        @Expose
        @SerializedName("id")
        private String id;
        @Expose
        @SerializedName("created_at")
        private String created_at;

        public String getPlantationName() {
            return plantationName;
        }

        public void setPlantationName(String plantationName) {
            this.plantationName = plantationName;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getVillage() {
            return village;
        }

        public void setVillage(String village) {
            this.village = village;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getCompanyId() {
            return company_id;
        }

        public void setCompanyId(String company_id) {
            this.company_id = company_id;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCreatedAt() {
            return created_at;
        }

        public void setCreatedAt(String created_at) {
            created_at = created_at;
        }
    }
}
