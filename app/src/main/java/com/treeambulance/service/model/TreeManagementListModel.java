package com.treeambulance.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TreeManagementListModel {
    @Expose
    @SerializedName("result")
    private ArrayList<Result> result;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private String status;

    public ArrayList<Result> getResult() {
        return result;
    }

    public void setResult(ArrayList<Result> result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class Result {
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("tree_name")
        private String tree_name;
        @Expose
        @SerializedName("botanical_name")
        private String botanical_name;
        @Expose
        @SerializedName("company_id")
        private String company_id;
        @Expose
        @SerializedName("id")
        private String id;
        @Expose
        @SerializedName("created_at")
        private String created_at;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getTreeName() {
            return tree_name;
        }

        public void setTreeName(String tree_name) {
            this.tree_name = tree_name;
        }

        public String getBotanicalName() {
            return botanical_name;
        }

        public void setBotanicalName(String botanical_name) {
            this.botanical_name = botanical_name;
        }

        public String getCompanyId() {
            return company_id;
        }

        public void setCompanyId(String company_id) {
            this.company_id = company_id;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCreatedAt() {
            return created_at;
        }

        public void setCreatedAt(String created_at) {
            created_at = created_at;
        }
    }
}
