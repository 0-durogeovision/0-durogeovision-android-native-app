package com.treeambulance.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BookingDetailsListModel {
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("result")
    private Result result;
    @Expose
    @SerializedName("msg")
    private String msg;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class Result {
        @Expose
        @SerializedName("booking_details")
        private ArrayList<BookingDetails> bookingDetails;

        public ArrayList<BookingDetails> getBookingDetails() {
            return bookingDetails;
        }

        public void setBookingDetails(ArrayList<BookingDetails> bookingDetails) {
            this.bookingDetails = bookingDetails;
        }
    }

    public static class BookingDetails {
        @Expose
        @SerializedName("driver_status")
        private String driverStatus;
        @Expose
        @SerializedName("payment_mode")
        private String paymentMode;
        @Expose
        @SerializedName("collected_amt")
        private String collectedAmt;
        @Expose
        @SerializedName("payable_amt")
        private String payableAmt;
        @Expose
        @SerializedName("sourse")
        private String sourse;
        @Expose
        @SerializedName("destination")
        private String destination;
        @Expose
        @SerializedName("financialyear")
        private String financialyear;
        @Expose
        @SerializedName("area")
        private String area;
        @Expose
        @SerializedName("tripno")
        private String tripno;
        @Expose
        @SerializedName("package_id")
        private String packageId;
        @Expose
        @SerializedName("gst")
        private String gst;
        @Expose
        @SerializedName("tool_gate")
        private String toolGate;
        @Expose
        @SerializedName("driver_allowance")
        private String driverAllowance;
        @Expose
        @SerializedName("bill_need")
        private String billNeed;
        @Expose
        @SerializedName("tool_gate_amt")
        private String toolGateAmt;
        @Expose
        @SerializedName("closing_hrs")
        private String closingHrs;
        @Expose
        @SerializedName("price_per_kms")
        private String pricePerKms;
        @Expose
        @SerializedName("price_show")
        private String priceShow;
        @Expose
        @SerializedName("starting_hrs")
        private String startingHrs;
        @Expose
        @SerializedName("closing_km")
        private String closingKm;
        @Expose
        @SerializedName("starting_km")
        private String startingKm;
        @Expose
        @SerializedName("ac_r_nac")
        private String acRNac;
        @Expose
        @SerializedName("package")
        private String packageValue;
        @Expose
        @SerializedName("assigned")
        private String assigned;
        @Expose
        @SerializedName("cancel_reason")
        private String cancelReason;
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("regdate")
        private String regdate;
        @Expose
        @SerializedName("reference_no")
        private String referenceNo;
        @Expose
        @SerializedName("comments")
        private String comments;
        @Expose
        @SerializedName("source_id")
        private String sourceId;
        @Expose
        @SerializedName("journey_type")
        private String journeyType;
        @Expose
        @SerializedName("drop_place")
        private String dropPlace;
        @Expose
        @SerializedName("pickup")
        private String pickup;
        @Expose
        @SerializedName("landmark")
        private String landmark;
        @Expose
        @SerializedName("no_of_passenger")
        private String noOfPassenger;
        @Expose
        @SerializedName("trip_type")
        private String tripType;
        @Expose
        @SerializedName("reporting_to")
        private String reportingTo;
        @Expose
        @SerializedName("order_from")
        private String orderFrom;
        @Expose
        @SerializedName("engaged_by")
        private String engagedBy;
        @Expose
        @SerializedName("pickup_address")
        private String pickupAddress;
        @Expose
        @SerializedName("driver")
        private String driver;
        @Expose
        @SerializedName("vehicle_id")
        private String vehicleId;
        @Expose
        @SerializedName("vehicle_type")
        private String vehicleType;
        @Expose
        @SerializedName("booking_ap")
        private String bookingAp;
        @Expose
        @SerializedName("booking_minute")
        private String bookingMinute;
        @Expose
        @SerializedName("booking_hour")
        private String bookingHour;
        @Expose
        @SerializedName("booking_date")
        private String bookingDate;
        @Expose
        @SerializedName("booking_type")
        private String bookingType;
        @Expose
        @SerializedName("mobile")
        private String mobile;
        @Expose
        @SerializedName("customername")
        private String customername;
        @Expose
        @SerializedName("customer_id")
        private String customerId;
        @Expose
        @SerializedName("customer_address")
        private String customerAddress;
        @Expose
        @SerializedName("customertype")
        private String customertype;
        @Expose
        @SerializedName("booking_code")
        private String bookingCode;
        @Expose
        @SerializedName("id")
        private String id;

        public String getDriverStatus() {
            return driverStatus;
        }

        public void setDriverStatus(String driverStatus) {
            this.driverStatus = driverStatus;
        }

        public String getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
        }

        public String getCollectedAmt() {
            return collectedAmt;
        }

        public void setCollectedAmt(String collectedAmt) {
            this.collectedAmt = collectedAmt;
        }

        public String getPayableAmt() {
            return payableAmt;
        }

        public void setPayableAmt(String payableAmt) {
            this.payableAmt = payableAmt;
        }

        public String getSourse() {
            return sourse;
        }

        public void setSourse(String sourse) {
            this.sourse = sourse;
        }

        public String getDestination() {
            return destination;
        }

        public void setDestination(String destination) {
            this.destination = destination;
        }

        public String getFinancialyear() {
            return financialyear;
        }

        public void setFinancialyear(String financialyear) {
            this.financialyear = financialyear;
        }

        public String getArea() {
            return area;
        }

        public void setArea(String area) {
            this.area = area;
        }

        public String getTripno() {
            return tripno;
        }

        public void setTripno(String tripno) {
            this.tripno = tripno;
        }

        public String getPackageId() {
            return packageId;
        }

        public void setPackageId(String packageId) {
            this.packageId = packageId;
        }

        public String getGst() {
            return gst;
        }

        public void setGst(String gst) {
            this.gst = gst;
        }

        public String getToolGate() {
            return toolGate;
        }

        public void setToolGate(String toolGate) {
            this.toolGate = toolGate;
        }

        public String getDriverAllowance() {
            return driverAllowance;
        }

        public void setDriverAllowance(String driverAllowance) {
            this.driverAllowance = driverAllowance;
        }

        public String getBillNeed() {
            return billNeed;
        }

        public void setBillNeed(String billNeed) {
            this.billNeed = billNeed;
        }

        public String getToolGateAmt() {
            return toolGateAmt;
        }

        public void setToolGateAmt(String toolGateAmt) {
            this.toolGateAmt = toolGateAmt;
        }

        public String getClosingHrs() {
            return closingHrs;
        }

        public void setClosingHrs(String closingHrs) {
            this.closingHrs = closingHrs;
        }

        public String getPricePerKms() {
            return pricePerKms;
        }

        public void setPricePerKms(String pricePerKms) {
            this.pricePerKms = pricePerKms;
        }

        public String getPriceShow() {
            return priceShow;
        }

        public void setPriceShow(String priceShow) {
            this.priceShow = priceShow;
        }

        public String getStartingHrs() {
            return startingHrs;
        }

        public void setStartingHrs(String startingHrs) {
            this.startingHrs = startingHrs;
        }

        public String getClosingKm() {
            return closingKm;
        }

        public void setClosingKm(String closingKm) {
            this.closingKm = closingKm;
        }

        public String getStartingKm() {
            return startingKm;
        }

        public void setStartingKm(String startingKm) {
            this.startingKm = startingKm;
        }

        public String getAcRNac() {
            return acRNac;
        }

        public void setAcRNac(String acRNac) {
            this.acRNac = acRNac;
        }

        public String getPackageValue() {
            return packageValue;
        }

        public void setPackageValue(String packageValue) {
            this.packageValue = packageValue;
        }

        public String getAssigned() {
            return assigned;
        }

        public void setAssigned(String assigned) {
            this.assigned = assigned;
        }

        public String getCancelReason() {
            return cancelReason;
        }

        public void setCancelReason(String cancelReason) {
            this.cancelReason = cancelReason;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getRegdate() {
            return regdate;
        }

        public void setRegdate(String regdate) {
            this.regdate = regdate;
        }

        public String getReferenceNo() {
            return referenceNo;
        }

        public void setReferenceNo(String referenceNo) {
            this.referenceNo = referenceNo;
        }

        public String getComments() {
            return comments;
        }

        public void setComments(String comments) {
            this.comments = comments;
        }

        public String getSourceId() {
            return sourceId;
        }

        public void setSourceId(String sourceId) {
            this.sourceId = sourceId;
        }

        public String getJourneyType() {
            return journeyType;
        }

        public void setJourneyType(String journeyType) {
            this.journeyType = journeyType;
        }

        public String getDropPlace() {
            return dropPlace;
        }

        public void setDropPlace(String dropPlace) {
            this.dropPlace = dropPlace;
        }

        public String getPickup() {
            return pickup;
        }

        public void setPickup(String pickup) {
            this.pickup = pickup;
        }

        public String getLandmark() {
            return landmark;
        }

        public void setLandmark(String landmark) {
            this.landmark = landmark;
        }

        public String getNoOfPassenger() {
            return noOfPassenger;
        }

        public void setNoOfPassenger(String noOfPassenger) {
            this.noOfPassenger = noOfPassenger;
        }

        public String getTripType() {
            return tripType;
        }

        public void setTripType(String tripType) {
            this.tripType = tripType;
        }

        public String getReportingTo() {
            return reportingTo;
        }

        public void setReportingTo(String reportingTo) {
            this.reportingTo = reportingTo;
        }

        public String getOrderFrom() {
            return orderFrom;
        }

        public void setOrderFrom(String orderFrom) {
            this.orderFrom = orderFrom;
        }

        public String getEngagedBy() {
            return engagedBy;
        }

        public void setEngagedBy(String engagedBy) {
            this.engagedBy = engagedBy;
        }

        public String getPickupAddress() {
            return pickupAddress;
        }

        public void setPickupAddress(String pickupAddress) {
            this.pickupAddress = pickupAddress;
        }

        public String getDriver() {
            return driver;
        }

        public void setDriver(String driver) {
            this.driver = driver;
        }

        public String getVehicleId() {
            return vehicleId;
        }

        public void setVehicleId(String vehicleId) {
            this.vehicleId = vehicleId;
        }

        public String getVehicleType() {
            return vehicleType;
        }

        public void setVehicleType(String vehicleType) {
            this.vehicleType = vehicleType;
        }

        public String getBookingAp() {
            return bookingAp;
        }

        public void setBookingAp(String bookingAp) {
            this.bookingAp = bookingAp;
        }

        public String getBookingMinute() {
            return bookingMinute;
        }

        public void setBookingMinute(String bookingMinute) {
            this.bookingMinute = bookingMinute;
        }

        public String getBookingHour() {
            return bookingHour;
        }

        public void setBookingHour(String bookingHour) {
            this.bookingHour = bookingHour;
        }

        public String getBookingDate() {
            return bookingDate;
        }

        public void setBookingDate(String bookingDate) {
            this.bookingDate = bookingDate;
        }

        public String getBookingType() {
            return bookingType;
        }

        public void setBookingType(String bookingType) {
            this.bookingType = bookingType;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getCustomername() {
            return customername;
        }

        public void setCustomername(String customername) {
            this.customername = customername;
        }

        public String getCustomerId() {
            return customerId;
        }

        public void setCustomerId(String customerId) {
            this.customerId = customerId;
        }

        public String getCustomerAddress() {
            return customerAddress;
        }

        public void setCustomerAddress(String customerAddress) {
            this.customerAddress = customerAddress;
        }

        public String getCustomertype() {
            return customertype;
        }

        public void setCustomertype(String customertype) {
            this.customertype = customertype;
        }

        public String getBookingCode() {
            return bookingCode;
        }

        public void setBookingCode(String bookingCode) {
            this.bookingCode = bookingCode;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
