package com.treeambulance.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EditCompanyModel {
    @Expose
    @SerializedName("result")
    private Result result;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private String status;



    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public static class Result {
        @Expose
        @SerializedName("company_id")
        private String companyId;

        @Expose
        @SerializedName("logo_image")
        private String logoName;

        public String getCompanyId() {
            return companyId;
        }

        public void setCompanyId(String companyId) {
            this.companyId = companyId;
        }

        public String getLogoName() {
            return logoName;
        }

        public void setLogoName(String logoName) {
            this.logoName = logoName;
        }
    }
}
