package com.treeambulance.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ProjectListDetailsModel {

    @Expose
    @SerializedName("result")
    private ArrayList<Result> result;
    @Expose
    @SerializedName("msg")
    private String msg;
    @Expose
    @SerializedName("status")
    private String status;

    public ArrayList<Result> getResult() {
        return result;
    }

    public void setResult(ArrayList<Result> result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class Result {
        @Expose
        @SerializedName("project_name")
        private String projectName;
        @Expose
        @SerializedName("longitude")
        private String longitude;
        @Expose
        @SerializedName("latitude")
        private String latitude;
        @Expose
        @SerializedName("rfid_code")
        private String rfidCode;
        @Expose
        @SerializedName("selfie")
        private String selfie;
        @Expose
        @SerializedName("photo2")
        private String photo2;
        @Expose
        @SerializedName("photo1")
        private String photo1;
        @Expose
        @SerializedName("notes")
        private String notes;
        @Expose
        @SerializedName("new_operator_name")
        private String newOperatorName;
        @Expose
        @SerializedName("tree_age")
        private String treeAge;
        @Expose
        @SerializedName("grith_size")
        private String grithSize;
        @Expose
        @SerializedName("health_status")
        private String healthStatus;
        @Expose
        @SerializedName("local_name")
        private String localName;
        @Expose
        @SerializedName("botanical_name")
        private String botanicalName;
        @Expose
        @SerializedName("new_location")
        private String newLocation;
        @Expose
        @SerializedName("distance")
        private String distance;
        @Expose
        @SerializedName("new_village")
        private String newVillage;
        @Expose
        @SerializedName("new_district")
        private String newDistrict;
        @Expose
        @SerializedName("new_state")
        private String newState;
        @Expose
        @SerializedName("date")
        private String date;
        @Expose
        @SerializedName("new_tree_no")
        private String newTreeNo;
        @Expose
        @SerializedName("survey_no")
        private String surveyNo;
        @Expose
        @SerializedName("company_id")
        private String companyId;
        @Expose
        @SerializedName("project_id")
        private String projectId;
        @Expose
        @SerializedName("id")
        private String id;

        public String getProjectName() {
            return projectName;
        }

        public void setProjectName(String projectName) {
            this.projectName = projectName;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getRfidCode() {
            return rfidCode;
        }

        public void setRfidCode(String rfidCode) {
            this.rfidCode = rfidCode;
        }

        public String getSelfie() {
            return selfie;
        }

        public void setSelfie(String selfie) {
            this.selfie = selfie;
        }

        public String getPhoto2() {
            return photo2;
        }

        public void setPhoto2(String photo2) {
            this.photo2 = photo2;
        }

        public String getPhoto1() {
            return photo1;
        }

        public void setPhoto1(String photo1) {
            this.photo1 = photo1;
        }

        public String getNotes() {
            return notes;
        }

        public void setNotes(String notes) {
            this.notes = notes;
        }

        public String getNewOperatorName() {
            return newOperatorName;
        }

        public void setNewOperatorName(String newOperatorName) {
            this.newOperatorName = newOperatorName;
        }

        public String getTreeAge() {
            return treeAge;
        }

        public void setTreeAge(String treeAge) {
            this.treeAge = treeAge;
        }

        public String getGrithSize() {
            return grithSize;
        }

        public void setGrithSize(String grithSize) {
            this.grithSize = grithSize;
        }

        public String getHealthStatus() {
            return healthStatus;
        }

        public void setHealthStatus(String healthStatus) {
            this.healthStatus = healthStatus;
        }

        public String getLocalName() {
            return localName;
        }

        public void setLocalName(String localName) {
            this.localName = localName;
        }

        public String getBotanicalName() {
            return botanicalName;
        }

        public void setBotanicalName(String botanicalName) {
            this.botanicalName = botanicalName;
        }

        public String getNewLocation() {
            return newLocation;
        }

        public void setNewLocation(String newLocation) {
            this.newLocation = newLocation;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getNewVillage() {
            return newVillage;
        }

        public void setNewVillage(String newVillage) {
            this.newVillage = newVillage;
        }

        public String getNewDistrict() {
            return newDistrict;
        }

        public void setNewDistrict(String newDistrict) {
            this.newDistrict = newDistrict;
        }

        public String getNewState() {
            return newState;
        }

        public void setNewState(String newState) {
            this.newState = newState;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getNewTreeNo() {
            return newTreeNo;
        }

        public void setNewTreeNo(String newTreeNo) {
            this.newTreeNo = newTreeNo;
        }

        public String getSurveyNo() {
            return surveyNo;
        }

        public void setSurveyNo(String surveyNo) {
            this.surveyNo = surveyNo;
        }

        public String getCompanyId() {
            return companyId;
        }

        public void setCompanyId(String companyId) {
            this.companyId = companyId;
        }

        public String getProjectId() {
            return projectId;
        }

        public void setProjectId(String projectId) {
            this.projectId = projectId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}