package com.treeambulance.service.views.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseActivity;
import com.treeambulance.service.databinding.ActivitySplashBinding;

import java.util.List;

import timber.log.Timber;

import static android.os.Build.VERSION.SDK_INT;

public class SplashActivity extends BaseActivity {

    private ActivitySplashBinding binding;
    private String company_id = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(activity, R.layout.activity_splash);
        binding.setLifecycleOwner(this);

        company_id = sharedHelper.getFromUser("company_id");

        new Handler(Looper.getMainLooper()).postDelayed(() -> {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                isStoragePermissionGranted();
            }

        }, commonFunction.SPLASH_TIME_OUT);
    }

    public void isStoragePermissionGranted() {
        if (SDK_INT >= Build.VERSION_CODES.R) {

            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.MANAGE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && Environment.isExternalStorageManager()) {
                Timber.i("Permission is granted");
                moveToNextActivity();
            } else {
                PermissionListener permissionlistener = new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        moveToNextActivity();
                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {
                        if (deniedPermissions.contains(Manifest.permission.MANAGE_EXTERNAL_STORAGE)
                                && !Environment.isExternalStorageManager()) {
                            requestPermission();
                        } else {
                            moveToNextActivity();
                        }
                    }
                };
                TedPermission.with(context)
                        .setPermissionListener(permissionlistener)
//                        .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                        .setPermissions(
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.MANAGE_EXTERNAL_STORAGE,
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.CAMERA)
                        .check();
            }
        } else {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                Timber.i("Permission is granted");
                moveToNextActivity();
            } else {
                PermissionListener permissionlistener = new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        moveToNextActivity();
                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {
                        Toast.makeText(getApplicationContext(), "Please permit all the permissions", Toast.LENGTH_LONG).show();
                        finish();
                    }
                };
                TedPermission.with(context)
                        .setPermissionListener(permissionlistener)
//                        .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                        .setPermissions(
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.CAMERA)
                        .check();
            }
        }
    }

    private void requestPermission() {
        if (SDK_INT >= Build.VERSION_CODES.R) {
            try {
                Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
                intent.addCategory("android.intent.category.DEFAULT");
                intent.setData(Uri.parse(String.format("package:%s", getApplication().getPackageName())));
                startActivityForResult(intent, commonFunction.REQUEST_CODE_MANAGE_EXTERNAL_STORAGE);
            } catch (Exception e) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                startActivityForResult(intent, commonFunction.REQUEST_CODE_MANAGE_EXTERNAL_STORAGE);
            }
        }
    }

    private void moveToNextActivity() {
        if (company_id != null && !company_id.equalsIgnoreCase("")) {
            setIntent(new Intent(activity, HomeActivity.class), 2);
        } else {
            setIntent(new Intent(activity, SelectCompanyListActivity.class), 2);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == commonFunction.REQUEST_CODE_MANAGE_EXTERNAL_STORAGE) {
            if (SDK_INT >= Build.VERSION_CODES.R) {
                if (Environment.isExternalStorageManager()) {
                    moveToNextActivity();
                    // perform action when allow permission success
                } else {
                    Toast.makeText(this, "Allow permission for storage access!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }
    }

}