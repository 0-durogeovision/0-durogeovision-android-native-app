package com.treeambulance.service.views.activity;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseActivity;
import com.treeambulance.service.databinding.ActivityHomeBinding;
import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.model.CompanyDetailsModel;
import com.treeambulance.service.other.AppLocationService;
import com.treeambulance.service.presenter.HomePresenter;
import com.treeambulance.service.views.bottomSheetDialogFragment.LoginPasswordBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.ModesBottomSheetFragment;
import com.treeambulance.service.views.fragment.AllProjectsFragment;
import com.treeambulance.service.views.fragment.DuroTekModeFragment;
import com.treeambulance.service.views.fragment.MaintenanceFragment;
import com.treeambulance.service.views.fragment.PlantationFragment;
import com.treeambulance.service.views.fragment.PlantationMaintenanceFragment;
import com.treeambulance.service.views.fragment.PlantationRetrievalFragment;
import com.treeambulance.service.views.fragment.ProjectHomeFragment;
import com.treeambulance.service.views.fragment.SupervisorModeFragment;
import com.treeambulance.service.views.fragment.SurveyingFirstFragment;
import com.treeambulance.service.views.fragment.TransplantationFirstFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import timber.log.Timber;

import static com.treeambulance.service.other.Utiles.isNetworkAvailable;

public class HomeActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener,
        View.OnClickListener, ModesBottomSheetFragment.ModesOnClickListener, LoginPasswordBottomSheetFragment.LoginPasswordOnClickListener,
        HomePresenter.ContactInterface {

    ActivityHomeBinding binding;

    private String str_back = "0";
    private boolean doubleBackToExitPressedOnce = false;
    private HomePresenter homePresenter;
    private String str_action = "";
    private CompanyDetailsModel companyDetailsModel;
    private ArrayList<String> GPSDeviceList, GPSFromBluetooth, GPSSelectedBluetooth;
    private HashMap<String, String> getCompanyDetailsHashMap;
    private Set<BluetoothDevice> setBluetoothDevice;
    AppLocationService appLocationService;
    Location gpsLocation;
    LocationManager locationManager;
    Context mContext;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(activity, R.layout.activity_home);
        binding.setClickListener(this);
        binding.incAppBarMain.setClickListener(this);
        binding.incAppBarMain.setLifecycleOwner(this);
        binding.setLifecycleOwner(this);

        homePresenter = new HomePresenter(this);

        getCompanyDetailsHashMap = new HashMap<>();

        GPSDeviceList = new ArrayList<>();
        GPSFromBluetooth = new ArrayList<>();
        GPSSelectedBluetooth = new ArrayList<>();

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }
        setBluetoothDevice = mBluetoothAdapter.getBondedDevices();

        hitGetCompanyDetails();
        addBluetoothItemsToList();

        companyDetailsModel = new CompanyDetailsModel();

        binding.nvHome.setNavigationItemSelectedListener(this);
        mContext = this;
        locationManager = (LocationManager) context
                .getSystemService(LOCATION_SERVICE);

        appLocationService = new AppLocationService(
                this);

        gpsLocation = appLocationService
                .getLocation(LocationManager.GPS_PROVIDER,this);



        moveToFragment(new ProjectHomeFragment(), R.id.flHomeActivity, true);
    }

    private void addBluetoothItemsToList() {
        List<BluetoothDevice> list = new ArrayList<>(setBluetoothDevice);

        for (BluetoothDevice bluetoothDevice : list) {

            GPSFromBluetooth.add(bluetoothDevice.getAddress());
        }

        Timber.i("addBluetoothItemsToList: GPSFromBluetooth: %s", new Gson().toJson(GPSFromBluetooth));

    }

    private void hitGetCompanyDetails() {

        getCompanyDetailsHashMap.put("action", "company_details");
        getCompanyDetailsHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            homePresenter.getCompanyDetails(getCompanyDetailsHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            /*case R.id.nav_create_company:
                str_back = "1";
                moveToFragment(new CreateCompanyFragment(), android.R.id.content, false);
                break;*/
            /*case R.id.nav_create_project:
                str_back = "1";
                moveToFragment(new AddProjectFragment(), android.R.id.content, false);
                break;*/
            case R.id.nav_all_projects:
                str_back = "1";
                moveToFragment(new AllProjectsFragment(), android.R.id.content, false);
                break;
            /*case R.id.nav_manage_operator:
                str_back = "1";
                moveToFragment(new ManageOperatorFragment(), android.R.id.content, false);
                break;*/
            case R.id.nav_surveying:
                str_back = "1";
                moveToFragment(new SurveyingFirstFragment(), android.R.id.content, false);
                break;
            case R.id.nav_transplantation:
                str_back = "1";
                if (gpsLocation != null) {
                    moveToFragment(new TransplantationFirstFragment(), android.R.id.content, false);
                }
                else{
                    if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        commonFunction.showSettingsAlert(mContext);
                    }else{
                        moveToFragment(new TransplantationFirstFragment(), android.R.id.content, false);
                    }
                }
                break;
            case R.id.nav_maintenance:
                str_back = "1";
                moveToFragment(new MaintenanceFragment(), android.R.id.content, false);
                break;
            case R.id.nav_plantation:
                str_back = "1";
                moveToFragment(new PlantationFragment(), android.R.id.content, false);
                break;
            case R.id.nav_plantation_maintenance:
                str_back = "1";
                moveToFragment(new PlantationMaintenanceFragment(), android.R.id.content, false);
                break;
            case R.id.nav_data_retrieval:
                /*if (GPSDeviceList != null && !GPSDeviceList.isEmpty()) {
                    str_back = "1";
                    moveToFragment(new DataRetrievalFragment(GPSDeviceList), android.R.id.content, false);
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Project is Empty " + getResources().getString(R.string.something_wrong));
                }*/
                if (GPSSelectedBluetooth != null && !GPSSelectedBluetooth.isEmpty()) {
                    str_back = "1";
//                    moveToFragment(new DataRetrievalFragment(GPSSelectedBluetooth), android.R.id.content, false);
                    setIntent(new Intent(activity, DataRetrievalActivity.class), 1);
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Project is Empty " + getResources().getString(R.string.something_wrong));
                }
                break;
            case R.id.nav_plantation_retrieval:
//                if (GPSSelectedBluetooth != null && !GPSSelectedBluetooth.isEmpty()) {
                    str_back = "1";
//                    moveToFragment(new DataRetrievalFragment(GPSSelectedBluetooth), android.R.id.content, false);
                    setIntent(new Intent(activity, PlantationRetrievalActivity.class), 1);
//                } else {
//                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Project is Empty " + getResources().getString(R.string.something_wrong));
//                }
                break;
        }

        binding.dlHome.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (binding.dlHome.isDrawerOpen(GravityCompat.START)) {
            binding.dlHome.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        str_back = "0";
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivNavMenu:
                binding.dlHome.openDrawer(GravityCompat.START);
                break;
            case R.id.ivSubMenu:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                bottomSheetDialogFragment = new ModesBottomSheetFragment(this);
                bottomSheetDialogFragment.show(fragmentManager, "modes_dialog_sheet");
                break;
        }
    }

    /*@Override
    public void reportsClickListener(String toFragment) {
        switch (toFragment) {
            case "survey_report": {
                str_back = "1";
                moveToFragment(new SurveyReportFragment(), android.R.id.content, false);
            }
            break;
            case "transplantation_report": {
                str_back = "1";
                moveToFragment(new TransplantationReportFragment(), android.R.id.content, false);
            }
            break;
            case "maintenance_report": {
                str_back = "1";
                moveToFragment(new MaintenanceReportFragment(), android.R.id.content, false);
            }
            break;
            case "consolidated_report": {
                str_back = "1";
                moveToFragment(new ConsolidatedReportFragment(), android.R.id.content, false);
            }
            break;
        }
    }*/

    @Override
    public void modesClickListener(String toFragment) {
        switch (toFragment) {
            case "supervisor_mode": {
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                bottomSheetDialogFragment = new LoginPasswordBottomSheetFragment("supervisor_mode", this);
                bottomSheetDialogFragment.show(fragmentManager, "supervisor_mode_dialog_sheet");
            }
            break;
            case "durotek_mode": {
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                bottomSheetDialogFragment = new LoginPasswordBottomSheetFragment("durotek_mode", this);
                bottomSheetDialogFragment.show(fragmentManager, "durotek_mode_dialog_sheet");
            }
            break;
        }
    }

    @Override
    public void loginPasswordClickListener(String action, HashMap<String, String> loginPasswordHashMap) {

        str_action = action;

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            homePresenter.getLoginPassword(loginPasswordHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    @Override
    public void onSuccessLoginPassword(CommonModel model) {
        commonFunction.dismissLoader();
        if (str_action.equalsIgnoreCase("supervisor_mode")) {
//            commonFunction.SuccessAlert(activity, getResources().getString(R.string.success_title), "Successfully Logged In to Supervisor Mode");
            moveToFragment(new SupervisorModeFragment(), android.R.id.content, false);
        } else if (str_action.equalsIgnoreCase("durotek_mode")) {
//            commonFunction.SuccessAlert(activity, getResources().getString(R.string.success_title), "Successfully Logged In to Durotek Mode");
            moveToFragment(new DuroTekModeFragment(), android.R.id.content, false);
        }
    }

    @Override
    public void onErrorLoginPassword(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessCompanyDetails(CompanyDetailsModel companyDetailsModel) {
        commonFunction.dismissLoader();

        Timber.i("onSuccessCompanyDetails: companyDetailsModel: %s", new Gson().toJson(companyDetailsModel));

        if (companyDetailsModel != null && companyDetailsModel.getResult() != null) {
            this.companyDetailsModel = companyDetailsModel;
        }

        /*if (!companyDetailsModel.getResult().getGpsList().isEmpty()) {
            for (CompanyDetailsModel.GpsList list : companyDetailsModel.getResult().getGpsList()) {
                GPSDeviceList.add(list.getAddress());
            }
        } else {
            commonFunction.FailureAlert(activity, context.getString(R.string.error_title), "GPS Device List is Empty");
        }*/
        if (!companyDetailsModel.getResult().getGpsList().isEmpty()) {
            for (CompanyDetailsModel.GpsList list : companyDetailsModel.getResult().getGpsList()) {
                GPSDeviceList.add(list.getAddress());
            }
        } else {
            commonFunction.FailureAlert(activity, context.getString(R.string.error_title), "GPS Device List is Empty");
        }

        for (String strAddress : GPSDeviceList) {
            if (GPSFromBluetooth.contains(strAddress)) {
                GPSSelectedBluetooth.add(strAddress);
            }
        }

        Timber.i("onSuccessCompanyDetails: GPSSelectedBluetooth: %s", new Gson().toJson(GPSSelectedBluetooth));
    }

    @Override
    public void onErrorCompanyDetails(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        homePresenter.onDispose();
    }
}