package com.treeambulance.service.views.bottomSheetDialogFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseBottomSheet;
import com.treeambulance.service.databinding.FragmentStatusMultiListBottomSheetBinding;
import com.treeambulance.service.model.StatusListModel;
import com.treeambulance.service.model.StatusRangeModel;
import com.treeambulance.service.views.adapter.StatusMultiListAdapter;
import com.treeambulance.service.views.adapter.StatusMultiRangeAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class StatusRangeMultiListBottomSheetFragment extends BaseBottomSheet implements View.OnClickListener, StatusMultiRangeAdapter.OnStatusMultiListSelected {
    private FragmentStatusMultiListBottomSheetBinding binding;
    private ArrayList<StatusRangeModel> statusRangeModels;
    private StatusRangeMultiListBottomSheetFragment.StatusRangeMultiClickListener statusRangeMultiClickListener;
    private StatusMultiRangeAdapter statusMultiListAdapter;

    public StatusRangeMultiListBottomSheetFragment() {
        // Required empty public constructor
    }

    public StatusRangeMultiListBottomSheetFragment(ArrayList<StatusRangeModel> statusListResultsModel, StatusRangeMultiListBottomSheetFragment.StatusRangeMultiClickListener statusRangeMultiClickListener) {
        this.statusRangeMultiClickListener = statusRangeMultiClickListener;
        this.statusRangeModels = statusListResultsModel;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_status_multi_list_bottom_sheet,
                container,
                false
        );

        binding.setClickListener(this);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        statusMultiListAdapter = new StatusMultiRangeAdapter(statusRangeModels, this);
        binding.rvStatusList.setAdapter(statusMultiListAdapter);
    }

    @Override
    public void onStatusSelected(int position, boolean isChecked) {
        statusRangeModels.get(position).setChecked(isChecked);
        statusMultiListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btAdd: {
                statusRangeMultiClickListener.onStatusMultiClick(statusRangeModels);
                dismiss();
            }
            break;
            case R.id.btSelectAll: {

                for (int i = 0; i < statusRangeModels.size(); i++) {
                    statusRangeModels.get(i).setChecked(true);
                }

                statusRangeMultiClickListener.onStatusMultiClick(statusRangeModels);
                dismiss();
            }
            break;
            case R.id.btDeselectAll: {

                for (int i = 0; i < statusRangeModels.size(); i++) {
                    statusRangeModels.get(i).setChecked(false);
                }

                statusRangeMultiClickListener.onStatusMultiClick(statusRangeModels);
                dismiss();
            }
            break;
        }
    }

    public interface StatusRangeMultiClickListener {
        void onStatusMultiClick(ArrayList<StatusRangeModel> statusListModel);
    }
}


