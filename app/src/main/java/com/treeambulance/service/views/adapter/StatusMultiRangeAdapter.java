package com.treeambulance.service.views.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.treeambulance.service.R;
import com.treeambulance.service.databinding.AdapterMultiListBinding;
import com.treeambulance.service.model.StatusListModel;
import com.treeambulance.service.model.StatusRangeModel;

import java.util.ArrayList;

public class StatusMultiRangeAdapter extends RecyclerView.Adapter<StatusMultiListAdapter.ViewHolder> {

    private ArrayList<StatusRangeModel> statusRangeModel;
    private OnStatusMultiListSelected onStatusMultiListSelected;

    public StatusMultiRangeAdapter(ArrayList<StatusRangeModel> statusRangeModel, OnStatusMultiListSelected onStatusMultiListSelected) {
        this.statusRangeModel = statusRangeModel;
        this.onStatusMultiListSelected = onStatusMultiListSelected;
    }

    @NonNull
    @Override
    public StatusMultiListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new StatusMultiListAdapter.ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_multi_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull StatusMultiListAdapter.ViewHolder holder, int position) {
        holder.binding.tvItemName.setText(statusRangeModel.get(position).getStatus());
        holder.binding.cbItem.setChecked(statusRangeModel.get(position).isChecked());

        holder.itemView.setOnClickListener(view -> {
            if (statusRangeModel.get(position).isChecked()) {
                onStatusMultiListSelected.onStatusSelected(position, false);
            } else if (!statusRangeModel.get(position).isChecked()) {
                onStatusMultiListSelected.onStatusSelected(position, true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return statusRangeModel.size();
    }

    public interface OnStatusMultiListSelected {
        public void onStatusSelected(int position, boolean isChecked);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        AdapterMultiListBinding binding;

        public ViewHolder(@NonNull AdapterMultiListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
