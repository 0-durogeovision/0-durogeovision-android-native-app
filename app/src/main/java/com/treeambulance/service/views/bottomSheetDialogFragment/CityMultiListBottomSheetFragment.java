package com.treeambulance.service.views.bottomSheetDialogFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseBottomSheet;
import com.treeambulance.service.databinding.FragmentCityMultiListBottomSheetBinding;
import com.treeambulance.service.model.CityModel;
import com.treeambulance.service.views.adapter.CityMultiListAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class CityMultiListBottomSheetFragment extends BaseBottomSheet implements View.OnClickListener, CityMultiListAdapter.OnCityMultiListSelected {
    private FragmentCityMultiListBottomSheetBinding binding;
    private ArrayList<CityModel> cityListResultsModel;
    private ArrayList<CityModel> cityAllListResultsModel;
    private CityListMultiClickListener cityListMultiClickListener;
    private CityMultiListAdapter cityMultiListAdapter;

    public CityMultiListBottomSheetFragment() {
        // Required empty public constructor
    }

    public CityMultiListBottomSheetFragment(ArrayList<CityModel> cityListResultsModel, CityListMultiClickListener cityListMultiClickListener) {
        this.cityListResultsModel = cityListResultsModel;
        this.cityListMultiClickListener = cityListMultiClickListener;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_city_multi_list_bottom_sheet,
                container,
                false
        );

        binding.setClickListener(this);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        cityAllListResultsModel = new ArrayList<>();

        cityMultiListAdapter = new CityMultiListAdapter(cityListResultsModel, this);
        binding.rvCityList.setAdapter(cityMultiListAdapter);
    }

    @Override
    public void onCitySelected(int position, boolean isChecked) {
        cityListResultsModel.get(position).setChecked(isChecked);
        cityMultiListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btAdd: {
                cityListMultiClickListener.onCityMultiClick(cityListResultsModel);
                dismiss();
            }
            break;
            case R.id.btSelectAll: {
                for (int i = 0; i < cityListResultsModel.size(); i++) {
                    cityListResultsModel.get(i).setChecked(true);
                }
                cityListMultiClickListener.onCityMultiClick(cityListResultsModel);
                dismiss();
            }
            break;
            case R.id.btDeselectAll: {
                for (int i = 0; i < cityListResultsModel.size(); i++) {
                    cityListResultsModel.get(i).setChecked(false);
                }
                cityListMultiClickListener.onCityMultiClick(cityListResultsModel);
                dismiss();
            }
            break;
        }
    }

    public interface CityListMultiClickListener {
        void onCityMultiClick(ArrayList<CityModel> cityListResultsModel);
    }
}