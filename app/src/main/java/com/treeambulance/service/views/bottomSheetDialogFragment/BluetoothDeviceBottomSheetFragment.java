package com.treeambulance.service.views.bottomSheetDialogFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseBottomSheet;
import com.treeambulance.service.databinding.FragmentBluetoothDeviceBottomSheetBinding;
import com.treeambulance.service.model.BluetoothDeviceListModel;
import com.treeambulance.service.views.adapter.BluetoothDeviceAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class BluetoothDeviceBottomSheetFragment extends BaseBottomSheet implements View.OnClickListener,
        BluetoothDeviceAdapter.OnBluetoothDeviceSelected {

    private FragmentBluetoothDeviceBottomSheetBinding binding;
    private ArrayList<BluetoothDeviceListModel> bluetoothDeviceListModel = new ArrayList<>();
    private BluetoothDeviceAdapter bluetoothDeviceAdapter;
    private AddSelectedDrives addSelectedDrives;

    public BluetoothDeviceBottomSheetFragment() {
        // Required empty public constructor
    }

    public BluetoothDeviceBottomSheetFragment(ArrayList<BluetoothDeviceListModel> bluetoothDeviceListModel, AddSelectedDrives addSelectedDrives) {
        this.bluetoothDeviceListModel = bluetoothDeviceListModel;
        this.addSelectedDrives = addSelectedDrives;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_bluetooth_device_bottom_sheet,
                container,
                false
        );

        binding.setClickListener(this);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bluetoothDeviceAdapter = new BluetoothDeviceAdapter(bluetoothDeviceListModel, this);
        binding.rvBluetoothDeviceList.setAdapter(bluetoothDeviceAdapter);
    }

    @Override
    public void onSelected(int position, boolean isChecked) {
        bluetoothDeviceListModel.get(position).setChecked(isChecked);
        bluetoothDeviceAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btAdd: {
                addSelectedDrives.addDrives(bluetoothDeviceListModel);
                dismiss();
            }
            break;
        }
    }

    public interface AddSelectedDrives {
        void addDrives(ArrayList<BluetoothDeviceListModel> bluetoothDeviceListModel);
    }
}