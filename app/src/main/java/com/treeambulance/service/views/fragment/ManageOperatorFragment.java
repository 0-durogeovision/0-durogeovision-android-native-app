package com.treeambulance.service.views.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.Interface.OnItemClickListener;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseFragment;
import com.treeambulance.service.databinding.FragmentManageOperatorBinding;
import com.treeambulance.service.model.CommonModel;
import com.treeambulance.service.model.CreateCompanyModel;
import com.treeambulance.service.model.OperatorListModel;
import com.treeambulance.service.presenter.ManageOperatorPresenter;
import com.treeambulance.service.views.adapter.OperatorListAdapter;
import com.treeambulance.service.views.bottomSheetDialogFragment.CustomDialogBottomSheetFragment;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

public class ManageOperatorFragment extends BaseFragment implements View.OnClickListener,
        ManageOperatorPresenter.ContactInterface, OnItemClickListener,
        CustomDialogBottomSheetFragment.CustomDialogOnButtonClick {

    FragmentManageOperatorBinding binding;
    ManageOperatorPresenter manageOperatorPresenter;

    private HashMap<String, String> getOperatorListHashMap;
    private HashMap<String, String> getCreateOperatorHashMap;
    private HashMap<String, String> getDeleteOperatorHashMap;

    private OperatorListModel operatorListModel;
    private int position = 0;

    public ManageOperatorFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_manage_operator, container, false);
        binding.setClickListener(this);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        manageOperatorPresenter = new ManageOperatorPresenter(this);

        getOperatorListHashMap = new HashMap<>();
        getCreateOperatorHashMap = new HashMap<>();
        getDeleteOperatorHashMap = new HashMap<>();

        operatorListModel = new OperatorListModel();
        hitGetOperatorList();
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btAdd: {
                validator();
            }
            break;
            case R.id.ivBackManageOperator: {
                hideSoftKeyBoard(context, v);
                fragmentManager.popBackStackImmediate();
            }
            break;
        }
    }

    private void hitGetOperatorList() {

        getOperatorListHashMap.put("action", "operators_list");
        getOperatorListHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            manageOperatorPresenter.getOperatorList(getOperatorListHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void deleteProject(int position) {
        if (bottomSheetDialogFragment != null) {
            bottomSheetDialogFragment.dismiss();
        }
        bottomSheetDialogFragment = new CustomDialogBottomSheetFragment(
                "Make sure want to delete " + operatorListModel.getResult().get(position).getName()
                        + " ?", this);
        bottomSheetDialogFragment.show(fragmentManager, "custom_dialog_sheet");
    }

    private void validator() {
        if (!commonFunction.NullPointerValidator(binding.etOperatorName.getText().toString().trim())) {
            binding.etOperatorName.requestFocus();
            commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Enter the Operator Name");
        } else {

            getCreateOperatorHashMap.put("action", "create_operators");
            getCreateOperatorHashMap.put("operator_name", binding.etOperatorName.getText().toString().trim());
            getCreateOperatorHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

            if (isNetworkAvailable(activity)) {
                commonFunction.showLoader(activity);
                manageOperatorPresenter.getCreateOperator(getCreateOperatorHashMap);
            } else {
                commonFunction.showNoNetwork(activity);
            }
        }

    }

    @Override
    public void onSuccessOperatorList(OperatorListModel operatorListModel) {
        commonFunction.dismissLoader();
        if (operatorListModel != null && operatorListModel.getResult() != null && !operatorListModel.getResult().isEmpty()) {
            this.operatorListModel = operatorListModel;
            binding.rvOperatorsList.setAdapter(new OperatorListAdapter(operatorListModel.getResult(), this));
            binding.tvNoData.setVisibility(View.GONE);
            binding.rvOperatorsList.setVisibility(View.VISIBLE);
        } else {
            binding.tvNoData.setVisibility(View.VISIBLE);
            binding.rvOperatorsList.setVisibility(View.GONE);
        }
    }

    @Override
    public void onErrorOperatorList(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessCreateOperator(CreateCompanyModel createCompanyModel) {
        commonFunction.dismissLoader();
        binding.etOperatorName.setText("");
        commonFunction.SuccessAlert(activity, context.getString(R.string.success_title), createCompanyModel.getMsg());
        hitGetOperatorList();
    }

    @Override
    public void onErrorCreateOperator(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessDeleteProjects(CommonModel commonModel) {
        commonFunction.dismissLoader();
        commonFunction.SuccessAlert(activity, context.getString(R.string.success_title), commonModel.getMsg());
        hitGetOperatorList();
    }

    @Override
    public void onErrorDeleteProjects(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onItemClick(int position) {
        this.position = position;
        deleteProject(this.position);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        manageOperatorPresenter.onDispose();
    }

    @Override
    public void customDialogOnClick(boolean isPositive) {
        if (isPositive) {
            getDeleteOperatorHashMap.put("action", "delete_operator");
            getDeleteOperatorHashMap.put("company_id", sharedHelper.getFromUser("company_id"));
            getDeleteOperatorHashMap.put("operator_id", operatorListModel.getResult().get(position).getId());

            if (isNetworkAvailable(activity)) {
                commonFunction.showLoader(activity);
                manageOperatorPresenter.getDeleteOperator(getDeleteOperatorHashMap);
            } else {
                commonFunction.showNoNetwork(activity);
            }
        }
    }
}