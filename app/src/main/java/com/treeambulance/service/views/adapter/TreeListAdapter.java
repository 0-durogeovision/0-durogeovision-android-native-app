package com.treeambulance.service.views.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.treeambulance.service.Interface.OnItemClickListener;
import com.treeambulance.service.R;
import com.treeambulance.service.databinding.AdapterListBinding;
import com.treeambulance.service.databinding.AdapterTreeListBinding;
import com.treeambulance.service.model.TreeManagementListModel;

import java.util.ArrayList;

public class TreeListAdapter extends RecyclerView.Adapter<TreeListAdapter.ViewHolder> {

    private final ArrayList<TreeManagementListModel.Result> treeManagementListResultsModel;
    private TreeClickListener onTreeClickListener;
    private OnItemClickListener onItemClickListener;

    public TreeListAdapter(ArrayList<TreeManagementListModel.Result> treeManagementListResultsModel, OnItemClickListener onItemClickListener) {
        this.treeManagementListResultsModel = treeManagementListResultsModel;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_tree_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.setModel(treeManagementListResultsModel.get(position));
        holder.itemView.setOnClickListener(view -> onItemClickListener.onItemClick(position));
    }

    public interface TreeClickListener {
        public void treeOnClick(int position, String action);
    }

    @Override
    public int getItemCount() {
        return treeManagementListResultsModel.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public AdapterTreeListBinding binding;
        public ViewHolder(AdapterTreeListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}
