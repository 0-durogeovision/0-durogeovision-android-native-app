package com.treeambulance.service.views.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.treeambulance.service.R;
import com.treeambulance.service.databinding.AdapterMultiListBinding;
import com.treeambulance.service.model.StatusListModel;

import java.util.ArrayList;

public class StatusMultiListAdapter extends RecyclerView.Adapter<StatusMultiListAdapter.ViewHolder> {

    private ArrayList<StatusListModel> statusListModels;
    private OnStatusMultiListSelected onStatusMultiListSelected;

    public StatusMultiListAdapter(ArrayList<StatusListModel> statusCityModels, OnStatusMultiListSelected onStatusMultiListSelected) {
        this.statusListModels = statusCityModels;
        this.onStatusMultiListSelected = onStatusMultiListSelected;
    }

    @NonNull
    @Override
    public StatusMultiListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_multi_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull StatusMultiListAdapter.ViewHolder holder, int position) {
        holder.binding.tvItemName.setText(statusListModels.get(position).getFrom() + "% - " + statusListModels.get(position).getTo() + "%");
        holder.binding.cbItem.setChecked(statusListModels.get(position).isChecked());

        holder.itemView.setOnClickListener(view -> {
            if (statusListModels.get(position).isChecked()) {
                onStatusMultiListSelected.onStatusSelected(position, false);
            } else if (!statusListModels.get(position).isChecked()) {
                onStatusMultiListSelected.onStatusSelected(position, true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return statusListModels.size();
    }

    public interface OnStatusMultiListSelected {
        public void onStatusSelected(int position, boolean isChecked);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        AdapterMultiListBinding binding;

        public ViewHolder(@NonNull AdapterMultiListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
