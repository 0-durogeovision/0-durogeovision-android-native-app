package com.treeambulance.service.views.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.treeambulance.service.Interface.OnItemClickListener;
import com.treeambulance.service.R;
import com.treeambulance.service.databinding.AdapterCountryListBinding;
import com.treeambulance.service.databinding.AdapterOperatorListBinding;
import com.treeambulance.service.model.CountryListModel;
import com.treeambulance.service.model.OperatorListModel;

import java.util.ArrayList;

public class CountryListAdapter extends RecyclerView.Adapter<CountryListAdapter.ViewHolder> {

    private final ArrayList<CountryListModel.Result> countryListResultsModel;
    private final OnItemClickListener onItemClickListener;

    public CountryListAdapter(ArrayList<CountryListModel.Result> countryListResultsModel, OnItemClickListener onItemClickListener) {
        this.countryListResultsModel = countryListResultsModel;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public CountryListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CountryListAdapter.ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_country_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.setModel(countryListResultsModel.get(position));
        holder.itemView.setOnClickListener(view -> onItemClickListener.onItemClick(position));
    }

    @Override
    public int getItemCount() {
        return countryListResultsModel.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        AdapterCountryListBinding binding;

        public ViewHolder(AdapterCountryListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}

