package com.treeambulance.service.views.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.treeambulance.service.R;
import com.treeambulance.service.databinding.AdapterSelectedBluetoothDeviceBinding;
import com.treeambulance.service.model.BluetoothDeviceListModel;

import java.util.ArrayList;

public class SelectedBluetoothDeviceAdapter extends RecyclerView.Adapter<SelectedBluetoothDeviceAdapter.ViewHolder> {

    ArrayList<BluetoothDeviceListModel> bluetoothDeviceListModels;

    public SelectedBluetoothDeviceAdapter(ArrayList<BluetoothDeviceListModel> bluetoothDeviceListModels) {
        this.bluetoothDeviceListModels = bluetoothDeviceListModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_selected_bluetooth_device, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.setModel(bluetoothDeviceListModels.get(position));
    }

    @Override
    public int getItemCount() {
        return bluetoothDeviceListModels.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final AdapterSelectedBluetoothDeviceBinding binding;

        public ViewHolder(@NonNull AdapterSelectedBluetoothDeviceBinding adapterSelectedBluetoothDeviceBinding) {
            super(adapterSelectedBluetoothDeviceBinding.getRoot());
            this.binding = adapterSelectedBluetoothDeviceBinding;
        }
    }
}
