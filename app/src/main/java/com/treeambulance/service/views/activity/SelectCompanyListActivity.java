package com.treeambulance.service.views.activity;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.Interface.OnItemClickListener;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseActivity;
import com.treeambulance.service.databinding.ActivitySelectCompanyListBinding;
import com.treeambulance.service.model.CompanyListModel;
import com.treeambulance.service.presenter.SelectCompanyPresenter;
import com.treeambulance.service.views.adapter.CompanyListAdapter;
import com.treeambulance.service.views.fragment.CreateCompanyFragment;

import java.util.HashMap;

import static com.treeambulance.service.other.Utiles.isNetworkAvailable;

public class SelectCompanyListActivity extends BaseActivity implements View.OnClickListener, SelectCompanyPresenter.ContactInterface, OnItemClickListener {

    private ActivitySelectCompanyListBinding binding;

    private BluetoothAdapter bluetoothAdapter;
    private SelectCompanyPresenter selectCompanyPresenter;
    private HashMap<String, String> getCompanyListHashMap;
    private CompanyListModel companyListModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(activity, R.layout.activity_select_company_list);

        binding.setClickListener(this);
        binding.setLifecycleOwner(this);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!bluetoothAdapter.isEnabled()) {
            bluetoothAdapter.enable();
        }

        selectCompanyPresenter = new SelectCompanyPresenter(this);
        getCompanyListHashMap = new HashMap<>();
        companyListModel = new CompanyListModel();

        binding.srlCompanyList.setOnRefreshListener(() -> {
            hitCompanyList();
            binding.srlCompanyList.setRefreshing(false);
        });

        hitCompanyList();
    }

    private void hitCompanyList() {
        getCompanyListHashMap.put("action", "get_company_list");

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            selectCompanyPresenter.getCompanyList(getCompanyListHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivAddOperator: {
                moveToFragment(new CreateCompanyFragment(), android.R.id.content, false);
            }
            break;
        }
    }

    @Override
    public void onSuccessCompanyList(CompanyListModel companyListModel) {
        commonFunction.dismissLoader();
        this.companyListModel = companyListModel;

        if (this.companyListModel != null && this.companyListModel.getResult() != null && !this.companyListModel.getResult().isEmpty()) {

            binding.rvCompanyList.setAdapter(new CompanyListAdapter(companyListModel.getResult(), this));
            binding.rvCompanyList.setVisibility(View.VISIBLE);
            binding.tvNoData.setVisibility(View.GONE);
        } else {

            binding.rvCompanyList.setVisibility(View.GONE);
            binding.tvNoData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onErrorCompanyList(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
        binding.rvCompanyList.setVisibility(View.GONE);
        binding.tvNoData.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemClick(int position) {
        sharedHelper.putInUser("company_id", companyListModel.getResult().get(position).getId());
        sharedHelper.putInUser("company_name", companyListModel.getResult().get(position).getCompanyName());
        sharedHelper.putInUser("company_logo", companyListModel.getResult().get(position).getLogoName());
        setIntent(new Intent(activity, HomeActivity.class), 2);
    }

    @Override
    protected void onResume() {
        super.onResume();

        hitCompanyList();
    }

    /*@Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }*/
}