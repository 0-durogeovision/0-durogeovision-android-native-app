package com.treeambulance.service.views.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseFragment;
import com.treeambulance.service.databinding.FragmentPlantationMapViewBinding;
import com.treeambulance.service.databinding.FragmentProjectMapViewBinding;
import com.treeambulance.service.model.CompanyDetailsModel;
import com.treeambulance.service.model.ListPlantationModel;
import com.treeambulance.service.model.ListProjectsModel;
import com.treeambulance.service.model.PlantationListModel;
import com.treeambulance.service.model.ProjectListDetailsModel;
import com.treeambulance.service.model.SurveyListDetailsModel;
import com.treeambulance.service.presenter.PlantationMapViewPresenter;
import com.treeambulance.service.presenter.PlantationRetrievalPresenter;
import com.treeambulance.service.presenter.ProjectsMapViewPresenter;
import com.treeambulance.service.views.bottomSheetDialogFragment.AllPlantationBottomSheetFragment;
import com.treeambulance.service.views.bottomSheetDialogFragment.AllProjectsBottomSheetFragment;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import timber.log.Timber;

public class PlantationMapViewFragment extends BaseFragment implements View.OnClickListener,
        OnMapReadyCallback, PlantationRetrievalPresenter.ContactInterface, AllPlantationBottomSheetFragment.OnPlantationClickListener, PlantationMapViewPresenter.ContactInterface {

    private FragmentPlantationMapViewBinding binding;
    private PlantationMapViewPresenter plantationMapViewPresenter;
    private SupportMapFragment supportMapFragment;
    private GoogleMap googleMap;
    private HashMap<String, String> listProjectsHashMap;
    private HashMap<String, String> projectListDetailsHashMap;
    private HashMap<String, String> surveyListDetailsHashMap;
    private ListPlantationModel listPlantationModel;
    private PlantationListModel projectListDetailsModel;
    private SurveyListDetailsModel surveyListDetailsModel;
    private String projectId = "";
    private MarkerOptions markerOptionsTransplantation, markerOptionsSurvey;
    private LatLng latLng;
    private Marker markerTransplantation, markerSurvey;
    private List<Marker> markerTransplantationList = new ArrayList<>(), markerSurveyList = new ArrayList<>();

    public PlantationMapViewFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_plantation_map_view, container, false);

        binding.setClickListener(this);
        binding.setLifecycleOwner(this);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        supportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapFragment);
        supportMapFragment.getMapAsync(this);

        listProjectsHashMap = new HashMap<>();
        projectListDetailsHashMap = new HashMap<>();
        surveyListDetailsHashMap = new HashMap<>();

        listPlantationModel = new ListPlantationModel();
        projectListDetailsModel = new PlantationListModel();
        surveyListDetailsModel = new SurveyListDetailsModel();

        plantationMapViewPresenter = new PlantationMapViewPresenter(this);

        hitGetAllProjects();
    }

    private void hitGetAllProjects() {
        listProjectsHashMap.put("action", "list_plantation");
        listProjectsHashMap.put("company_id", sharedHelper.getFromUser("company_id"));

        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            plantationMapViewPresenter.getListPlantations(listProjectsHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void hitGetTransplantationListDetails() {
        projectListDetailsHashMap.put("action", "plantation_project_list_details");
        projectListDetailsHashMap.put("company_id", sharedHelper.getFromUser("company_id"));
        projectListDetailsHashMap.put("plantation_project_id", projectId);
        Timber.i("projectListDetailsHashMap: %s", projectListDetailsHashMap);
        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            plantationMapViewPresenter.getProjectListDetails(projectListDetailsHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void hitGetSurveyListDetails() {
        surveyListDetailsHashMap.put("action", "survey_list_details");
        surveyListDetailsHashMap.put("company_id", sharedHelper.getFromUser("company_id"));
        surveyListDetailsHashMap.put("project_id", projectId);
        Timber.i("surveyListDetailsHashMap: %s", surveyListDetailsHashMap);
        if (isNetworkAvailable(activity)) {
            commonFunction.showLoader(activity);
            plantationMapViewPresenter.getSurveyListDetails(surveyListDetailsHashMap);
        } else {
            commonFunction.showNoNetwork(activity);
        }
    }

    private void addTransplantationMarkers() {

        for (int i = 0; i < markerTransplantationList.size(); i++) {
            markerTransplantationList.get(i).remove();
        }

        if (projectListDetailsModel != null && projectListDetailsModel.getResult() != null && !projectListDetailsModel.getResult().isEmpty()) {

            binding.etTransplantationTreeCount.setText("" + projectListDetailsModel.getResult().size());
            for (PlantationListModel.Result list : projectListDetailsModel.getResult()) {
                if (commonFunction.NullPointerValidator(list.getLatitude()) && commonFunction.NullPointerValidator(list.getLongitude())) {

                    latLng = new LatLng(Double.parseDouble(list.getLatitude()), Double.parseDouble(list.getLongitude()));

                    markerOptionsTransplantation = new MarkerOptions()
                            .position(latLng)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                            .snippet(list.getNewTreeNo())
                            .title(list.getRfidCode());
                    markerTransplantation = googleMap.addMarker(markerOptionsTransplantation);
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
                    markerTransplantationList.add(markerTransplantation);

                    Timber.i("addMarkers markerTransplantation: %s", markerTransplantation.getTitle());
                }
            }
        } else {
            binding.etTransplantationTreeCount.setText("0");
        }
    }

    private void addSurveyMarkers() {

        for (int i = 0; i < markerSurveyList.size(); i++) {
            markerSurveyList.get(i).remove();
        }

        if (surveyListDetailsModel != null && surveyListDetailsModel.getResult() != null && !surveyListDetailsModel.getResult().isEmpty()) {

//            binding.etSurveyTreeCount.setText("" + surveyListDetailsModel.getResult().size());

            for (SurveyListDetailsModel.Result list : surveyListDetailsModel.getResult()) {
                if (commonFunction.NullPointerValidator(list.getLatitude()) && commonFunction.NullPointerValidator(list.getLongitude())) {

                    latLng = new LatLng(Double.parseDouble(list.getLatitude()), Double.parseDouble(list.getLongitude()));

                    markerOptionsSurvey = new MarkerOptions()
                            .position(latLng)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET))
                            .snippet(list.getSurveyNo())
                            .title(list.getRfidCode());
                    markerSurvey = googleMap.addMarker(markerOptionsSurvey);
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
                    markerSurveyList.add(markerSurvey);

                    Timber.i("addMarkers markerSurvey: %s", markerSurvey.getTitle());
                }
            }
        } else {
//            binding.etSurveyTreeCount.setText("0");
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.clear();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBackProjectMapView: {
                fragmentManager.popBackStackImmediate();
            }
            break;
            case R.id.etProjectName:
                if (bottomSheetDialogFragment != null) {
                    bottomSheetDialogFragment.dismiss();
                }
                if (listPlantationModel != null && listPlantationModel.getResult() != null && !listPlantationModel.getResult().isEmpty()) {
                    bottomSheetDialogFragment = new AllPlantationBottomSheetFragment(listPlantationModel.getResult(), this);
                    bottomSheetDialogFragment.show(fragmentManager, "all_project_list_sheet");
                } else {
                    commonFunction.displayErrorSnackbar(binding.getRoot(), context, "Project is Empty " + getResources().getString(R.string.something_wrong));
                }
                break;
        }
    }

    @Override
    public void onProjectClick(int position) {
        binding.etProjectName.setText(listPlantationModel.getResult().get(position).getProjectName());
        projectId = listPlantationModel.getResult().get(position).getId();
        hitGetTransplantationListDetails();
//        hitGetSurveyListDetails();
    }


    @Override
    public void onSuccessCompanyDetails(CompanyDetailsModel companyDetailsModel) {

    }

    @Override
    public void onErrorCompanyDetails(String error) {

    }

    @Override
    public void onSuccessListProjects(ListPlantationModel listPlantationModel) {
            commonFunction.dismissLoader();
            if (listPlantationModel != null && listPlantationModel.getResult() != null && !listPlantationModel.getResult().isEmpty()) {
                this.listPlantationModel = listPlantationModel;
            }
    }


    public void onErrorListProjects(String error) {
        commonFunction.dismissLoader();
        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessProjectListDetails(PlantationListModel projectListDetailsModel) {
        commonFunction.dismissLoader();
        if (projectListDetailsModel != null) {
            this.projectListDetailsModel = projectListDetailsModel;
            addTransplantationMarkers();
        }
    }

    @Override
    public void onErrorProjectListDetails(String error) {
        commonFunction.dismissLoader();

        binding.etTransplantationTreeCount.setText("0");
//        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onSuccessSurveyListDetails(SurveyListDetailsModel surveyListDetailsModel) {
        commonFunction.dismissLoader();
        if (surveyListDetailsModel != null) {
            this.surveyListDetailsModel = surveyListDetailsModel;
            addSurveyMarkers();
        }
    }

    @Override
    public void onErrorSurveyListDetails(String error) {
        commonFunction.dismissLoader();
//        binding.etSurveyTreeCount.setText("0");
//        commonFunction.FailureAlert(activity, context.getString(R.string.error_title), error);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        plantationMapViewPresenter.onDispose();
    }
}