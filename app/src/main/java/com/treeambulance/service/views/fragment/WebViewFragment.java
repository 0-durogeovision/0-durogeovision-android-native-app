package com.treeambulance.service.views.fragment;

import android.content.Context;
import android.os.Bundle;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintJob;
import android.print.PrintManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseFragment;

public class WebViewFragment extends BaseFragment {

    private WebView webView;
    private String webUrl = "";

    public WebViewFragment() {
        // Required empty public constructor
    }

    public WebViewFragment(String webUrl) {
        this.webUrl = webUrl;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_web_view, container, false);

        webView = view.findViewById(R.id.webView);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(true);

        webView.loadUrl(webUrl);
//        createWebPagePrint(webView);
    }

    public void createWebPagePrint(WebView webView) {
        PrintManager printManager = (PrintManager) context.getSystemService(Context.PRINT_SERVICE);
        PrintDocumentAdapter printAdapter = webView.createPrintDocumentAdapter();
        String jobName = "Survey Report Document";
        PrintAttributes.Builder builder = new PrintAttributes.Builder();
        builder.setMediaSize(PrintAttributes.MediaSize.ISO_A5);

        PrintJob printJob = printManager.print(jobName, printAdapter, builder.build());
        if (printJob.isCompleted()) {
            Log.e("print", "completed");
        } else if (printJob.isFailed()) {
            Log.e("print", "failed");
        } else if (printJob.isCancelled()) {
            Log.e("print", "cancelled");
        }
    }
}