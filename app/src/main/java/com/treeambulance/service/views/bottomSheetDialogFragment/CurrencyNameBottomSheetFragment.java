package com.treeambulance.service.views.bottomSheetDialogFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.Interface.OnItemClickListener;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseBottomSheet;
import com.treeambulance.service.databinding.FragmentCurrencyNameBottomSheetBinding;
import com.treeambulance.service.databinding.FragmentReportTypeBottomSheetBinding;
import com.treeambulance.service.model.CurrencyNameModel;
import com.treeambulance.service.model.ReportTypeModel;
import com.treeambulance.service.views.adapter.CurrencyNameAdapter;
import com.treeambulance.service.views.adapter.ReportTypeAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class CurrencyNameBottomSheetFragment extends BaseBottomSheet implements OnItemClickListener {

    private FragmentCurrencyNameBottomSheetBinding binding;
    private ArrayList<CurrencyNameModel> currencyNameModel = new ArrayList<>();
    private OnCurrencyNameClickListener onCurrencyNameClickListener;

    public CurrencyNameBottomSheetFragment(ArrayList<CurrencyNameModel> currencyNameModel, OnCurrencyNameClickListener onCurrencyNameClickListener) {
        // Required empty public constructor
        this.currencyNameModel = currencyNameModel;
        this.onCurrencyNameClickListener = onCurrencyNameClickListener;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_currency_name_bottom_sheet,
                container,
                false
        );
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.rvCurrencyName.setAdapter(new CurrencyNameAdapter(currencyNameModel, this));
    }

    @Override
    public void onItemClick(int position) {
        onCurrencyNameClickListener.onCurrencyNameClick(position);
        dismiss();
    }

    public interface OnCurrencyNameClickListener {
        public void onCurrencyNameClick(int position);
    }
}