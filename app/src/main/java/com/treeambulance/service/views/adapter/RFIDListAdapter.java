package com.treeambulance.service.views.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.treeambulance.service.Interface.OnItemClickListener;
import com.treeambulance.service.R;
import com.treeambulance.service.databinding.AdapterRfidListBinding;
import com.zebra.rfid.api3.TagData;

import java.util.ArrayList;

public class RFIDListAdapter extends RecyclerView.Adapter<RFIDListAdapter.ViewHolder> {

    ArrayList<TagData> tagDataList;
    ArrayList<String> tagData;
    OnItemClickListener onItemClickListener;

    /*public RFIDListAdapter(ArrayList<TagData> tagDataList, OnItemClickListener onItemClickListener) {
        this.tagDataList = tagDataList;
        this.onItemClickListener = onItemClickListener;
    }*/

    public RFIDListAdapter(ArrayList<String> tagData, OnItemClickListener onItemClickListener) {
        this.tagData = tagData;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_rfid_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
//        holder.binding.tvItemName.setText(tagDataList.get(position).getTagID());
        holder.binding.tvItemName.setText(tagData.get(position));
        holder.itemView.setOnClickListener(view -> onItemClickListener.onItemClick(position));
    }

    @Override
    public int getItemCount() {
//        return tagDataList.size();
        return tagData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final AdapterRfidListBinding binding;

        public ViewHolder(@NonNull AdapterRfidListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
