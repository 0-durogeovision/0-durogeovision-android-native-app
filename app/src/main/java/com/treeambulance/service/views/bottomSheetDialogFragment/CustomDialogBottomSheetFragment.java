package com.treeambulance.service.views.bottomSheetDialogFragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseBottomSheet;
import com.treeambulance.service.databinding.FragmentCustomDialogBottomSheetBinding;
import com.treeambulance.service.views.fragment.TreeManagementFragment;

import org.jetbrains.annotations.NotNull;

public class CustomDialogBottomSheetFragment extends BaseBottomSheet implements View.OnClickListener {
    CustomDialogOnButtonClick customDialogOnButtonClick;
    String message;
    FragmentCustomDialogBottomSheetBinding binding;

    public CustomDialogBottomSheetFragment(String message, CustomDialogOnButtonClick customDialogOnButtonClick) {
        this.message = message;
        this.customDialogOnButtonClick = customDialogOnButtonClick;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.tvMessage.setText(message);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_custom_dialog_bottom_sheet, container, false);
        binding.setClickListener(this);

        return binding.getRoot();
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btNegative:
                customDialogOnButtonClick.customDialogOnClick(false);
                dismiss();
                break;
            case R.id.btPositive:
                customDialogOnButtonClick.customDialogOnClick(true);
                dismiss();
                break;
        }
    }

    public interface CustomDialogOnButtonClick {
        public void customDialogOnClick(boolean isPositive);
    }
}