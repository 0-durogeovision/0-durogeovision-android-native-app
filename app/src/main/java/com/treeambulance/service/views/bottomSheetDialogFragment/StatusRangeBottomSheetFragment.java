package com.treeambulance.service.views.bottomSheetDialogFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.Interface.OnItemClickListener;
import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseBottomSheet;
import com.treeambulance.service.databinding.FragmentStatusRangeBottomSheetBinding;
import com.treeambulance.service.model.StatusRangeModel;
import com.treeambulance.service.views.adapter.StatusRangeAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class StatusRangeBottomSheetFragment extends BaseBottomSheet implements OnItemClickListener {

    private FragmentStatusRangeBottomSheetBinding binding;
    private ArrayList<StatusRangeModel> statusRangeModel = new ArrayList<>();
    private OnStatusRangeClickListener onStatusRangeClickListener;

    public StatusRangeBottomSheetFragment(ArrayList<StatusRangeModel> statusRangeModel, OnStatusRangeClickListener onStatusRangeClickListener) {
        // Required empty public constructor
        this.statusRangeModel = statusRangeModel;
        this.onStatusRangeClickListener = onStatusRangeClickListener;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_status_range_bottom_sheet,
                container,
                false
        );
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.rvStatusRange.setAdapter(new StatusRangeAdapter(statusRangeModel, this));
    }

    @Override
    public void onItemClick(int position) {
        onStatusRangeClickListener.onStatusRangeClick(position);
        dismiss();
    }

    public interface OnStatusRangeClickListener {
        public void onStatusRangeClick(int position);
    }
}
