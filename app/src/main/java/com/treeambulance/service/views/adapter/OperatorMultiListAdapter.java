package com.treeambulance.service.views.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.treeambulance.service.R;
import com.treeambulance.service.databinding.AdapterMultiListBinding;
import com.treeambulance.service.model.OperatorListModel;

import java.util.ArrayList;

public class OperatorMultiListAdapter extends RecyclerView.Adapter<OperatorMultiListAdapter.ViewHolder> {

    private ArrayList<OperatorListModel.Result> operatorListResultsModel;
    private OnOperatorMultiListSelected onOperatorMultiListSelected;

    public OperatorMultiListAdapter(ArrayList<OperatorListModel.Result> operatorListResultsModel, OnOperatorMultiListSelected onOperatorMultiListSelected) {
        this.operatorListResultsModel = operatorListResultsModel;
        this.onOperatorMultiListSelected = onOperatorMultiListSelected;
    }

    @NonNull
    @Override
    public OperatorMultiListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_multi_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull OperatorMultiListAdapter.ViewHolder holder, int position) {

        holder.binding.tvItemName.setText(operatorListResultsModel.get(position).getName());
        holder.binding.cbItem.setChecked(operatorListResultsModel.get(position).isChecked());

        holder.itemView.setOnClickListener(view -> {
            if (operatorListResultsModel.get(position).isChecked()) {
                onOperatorMultiListSelected.onOperatorSelected(position, false);
            } else if (!operatorListResultsModel.get(position).isChecked()) {
                onOperatorMultiListSelected.onOperatorSelected(position, true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return operatorListResultsModel.size();
    }

    public interface OnOperatorMultiListSelected {
        public void onOperatorSelected(int position, boolean isChecked);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        AdapterMultiListBinding binding;

        public ViewHolder(@NonNull AdapterMultiListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
