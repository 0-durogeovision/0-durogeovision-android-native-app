package com.treeambulance.service.views.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.treeambulance.service.R;
import com.treeambulance.service.model.ListProjectsModel;

import java.util.ArrayList;

public class AllProjectListAdapter extends RecyclerView.Adapter<AllProjectListAdapter.ViewHolder> {

    ArrayList<ListProjectsModel.Result> listProjectResultModel = new ArrayList<>();
    OnItemClickListener onItemClickListener;

    public AllProjectListAdapter(ArrayList<ListProjectsModel.Result> listProjectResultModel,
                                 OnItemClickListener onItemClickListener) {
        this.listProjectResultModel = listProjectResultModel;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public AllProjectListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AllProjectListAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_state_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AllProjectListAdapter.ViewHolder holder, int position) {
        holder.tvItemName.setText(listProjectResultModel.get(position).getProjectName() + " - " + listProjectResultModel.get(position).getVillage());
        holder.itemView.setOnClickListener(view -> onItemClickListener.onItemClick(position));
    }

    @Override
    public int getItemCount() {
        return listProjectResultModel.size();
    }

    public interface OnItemClickListener {
        public void onItemClick(int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvItemName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvItemName = itemView.findViewById(R.id.tvItemName);
        }
    }
}