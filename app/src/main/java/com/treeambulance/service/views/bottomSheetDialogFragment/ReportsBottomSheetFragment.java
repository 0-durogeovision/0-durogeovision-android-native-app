package com.treeambulance.service.views.bottomSheetDialogFragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.treeambulance.service.R;
import com.treeambulance.service.abstractClass.BaseBottomSheet;
import com.treeambulance.service.databinding.FragmentReportsBottomSheetBinding;
import com.treeambulance.service.views.fragment.SurveyReportFragment;

import org.jetbrains.annotations.NotNull;

public class ReportsBottomSheetFragment extends BaseBottomSheet implements View.OnClickListener {

    FragmentReportsBottomSheetBinding binding;
    ReportsOnClickListener reportsOnClickListener;

    public ReportsBottomSheetFragment() {
        // Required empty public constructor
    }

    public ReportsBottomSheetFragment(ReportsOnClickListener reportsOnClickListener) {
        this.reportsOnClickListener = reportsOnClickListener;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_reports_bottom_sheet, container, false);

        binding.setClickListener(this);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btSurveyReport: {
                reportsOnClickListener.reportsClickListener("survey_report");
                dismiss();
            }
            break;
            case R.id.btTransplantationReport: {
                reportsOnClickListener.reportsClickListener("transplantation_report");
                dismiss();
            }
            break;
            case R.id.btMaintenanceReport: {
                reportsOnClickListener.reportsClickListener("maintenance_report");
                dismiss();
            }
            break;
            case R.id.btConsolidatedReport: {
                reportsOnClickListener.reportsClickListener("consolidated_report");
                dismiss();
            }
            break;
        }
    }

    public interface ReportsOnClickListener{
        public void reportsClickListener(String toFragment);
    }
}