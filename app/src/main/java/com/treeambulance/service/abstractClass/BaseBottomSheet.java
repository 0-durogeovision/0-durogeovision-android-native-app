package com.treeambulance.service.abstractClass;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.treeambulance.service.other.CommonFunction;
import com.treeambulance.service.other.SharedHelper;

import org.greenrobot.eventbus.EventBus;

public abstract class BaseBottomSheet extends BottomSheetDialogFragment {

    public CommonFunction commonFunction;
    public SharedHelper sharedHelper;
    protected Activity activity;
    protected Context context;
    protected FragmentManager fragmentManager;
    protected EventBus eventBus;
    protected BottomSheetDialogFragment bottomSheetDialogFragment;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = getActivity();
        context = getContext();

        fragmentManager = ((AppCompatActivity) activity).getSupportFragmentManager();

        eventBus = EventBus.getDefault();
        commonFunction = new CommonFunction(activity.getApplication());
        sharedHelper = new SharedHelper(activity.getApplication());
    }

    protected void moveToFragment(Fragment fragment, int ids, Boolean first) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(ids, fragment);
        if (!first) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View bottomSheet = (View) this.getView().getParent();
        bottomSheet.setBackgroundTintMode(PorterDuff.Mode.CLEAR);
        bottomSheet.setBackgroundTintList(ColorStateList.valueOf(Color.TRANSPARENT));
        bottomSheet.setBackgroundColor(Color.TRANSPARENT);
    }

    public void hideSoftKeyBoard(Context context, View view) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (bottomSheetDialogFragment != null && bottomSheetDialogFragment.isAdded()) {
            bottomSheetDialogFragment.dismiss();
        }
    }
}
