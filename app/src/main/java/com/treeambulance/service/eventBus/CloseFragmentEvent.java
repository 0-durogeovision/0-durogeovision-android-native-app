package com.treeambulance.service.eventBus;

public class CloseFragmentEvent {
    public String closeFragment;

    public CloseFragmentEvent(String closeFragment) {
        this.closeFragment = closeFragment;
    }
}
